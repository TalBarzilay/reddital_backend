package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.controllers.requests.PostingRequest;
import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.SubReddit;
import com.project.reddital_backend.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.*;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PostMapperTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @Mock
    private CommentMapper commentMapper;

    @InjectMocks
    private PostMapper postMapper;

    final String title      = "i'm new here!";
    final String content    = "hello! just registered!";
    final Date time         = new Date();
    final Date update       = new Date(time.getTime() + 200000);

    final User user = User.builder().username("a").password("123456").email("a@a.a").build();
    final UserDto udto = UserDto.builder().username(user.getUsername()).build();
    final SubReddit sub = SubReddit.builder().name("r/askTal").build();

    final Comment comment1 = Comment.builder().content("agree").build();
    final Comment commentTo1 = Comment.builder().content("+1").responseTo(comment1).build();
    final Comment commentTo1_2 = Comment.builder().content("correct!").responseTo(comment1).build();

    final Comment comment2 = Comment.builder().content("I think you are wrong").build();



    final CommentDto comment1Dto = CommentDto.builder().content("agree").build();
    final CommentDto commentTo1Dto = CommentDto.builder().content("+1").responseTo(comment1Dto).build();
    final CommentDto commentTo1_2Dto = CommentDto.builder().content("correct!").responseTo(comment1Dto).build();

    final CommentDto comment2Dto = CommentDto.builder().content("I think you are wrong").build();


    final List<User> upvotes    = List.of(User.builder().build(), User.builder().build(), User.builder().build());
    final List<User> downvotes  = List.of(User.builder().build(), User.builder().build(), User.builder().build(), User.builder().build());

    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test toPostDto")
    public void toPostDto() {
        List<User> new_downs = new java.util.ArrayList<>(List.copyOf(downvotes));
        new_downs.add(user);

        Mockito.when(commentMapper.toCommentDto(comment1,  user.getUsername()))
                        .thenReturn(comment1Dto);

        Mockito.when(commentMapper.toCommentDto(comment2,  user.getUsername()))
                .thenReturn(comment2Dto);


        comment1.setComments(List.of(commentTo1, commentTo1_2));
        comment1Dto.setComments(List.of(commentTo1Dto, commentTo1_2Dto));

        Post post = Post.builder()
                .title(title)
                .content(content)
                .creation(time)
                .lastUpdated(update)
                .user(user)
                .subreddit(sub)
                .comments(List.of(comment1, comment2))
                .upVotes(upvotes)
                .isBlocked(true)
                .downVotes(new_downs)
                .build();

        comment1.setPostedOn(post);
        comment2.setPostedOn(post);

        final PostDto result = postMapper.toPostDto(post, user.getUsername());

        assertEquals(title, result.getTitle());
        assertEquals(content, result.getContent());
        assertEquals(time.getTime(), result.getCreation());
        assertEquals(update.getTime(), result.getLastUpdated());
        assertEquals(user.getUsername(), result.getUsername());
        assertEquals(sub.getName(), result.getSubReddit());

        assertEquals(post.isBlocked(), result.isBlocked());

        assertEquals(upvotes.size(), result.getUpVotes());
        assertEquals(new_downs.size(), result.getDownVotes());

        assertEquals(List.of("downvote"), result.getEmotes());

        assertEquals(List.of(comment1Dto, comment2Dto), result.getComments());
    }

    @Test
    @DisplayName("test toPostDto")
    public void toPostDto_request() {

        // Run the test
        PostingRequest request = PostingRequest.builder()
                .title(title)
                .content(content)
                .build();

        final PostDto result = postMapper.toPostDto(request, sub.getName(), udto.getUsername());

        assertEquals(title, result.getTitle());
        assertEquals(content, result.getContent());
        assertEquals(user.getUsername(), result.getUsername());
        assertEquals(sub.getName(), result.getSubReddit());

        assertEquals(0, result.getUpVotes());
        assertEquals(0, result.getDownVotes());
    }
}
