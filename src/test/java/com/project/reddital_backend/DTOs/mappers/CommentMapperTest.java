package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.controllers.requests.PostCommentRequest;
import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.services.CommentService;
import com.project.reddital_backend.services.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
class CommentMapperTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @Mock
    private PostService postService;

    @Mock
    private CommentService commentService;

    @InjectMocks
    private CommentMapper commentMapperUnderTest;

    final String logedUser  = "LeeroyJenkins";

    final User postee = User.builder().username("mrBanana").build();
    final UserDto posteeDto = UserDto.builder().username("mrBanana").build();

    final List<User> upvotes    = List.of(User.builder().build(), User.builder().username(logedUser).build(), User.builder().build());
    final List<User> downvotes  = List.of(User.builder().build(), User.builder().build(), User.builder().build(), User.builder().build());


    final Comment comment1 = Comment.builder().user(postee).content("agree").creation(new Date()).updated(new Date(new Date().getTime() + 10000)).build();
    final Comment commentTo1 = Comment.builder().content("+1").responseTo(comment1).build();
    final Comment commentTo1_2 = Comment.builder().content("correct!").responseTo(comment1).build();

    final CommentDto comment1Dto = CommentDto.builder().user(postee.getUsername()).content("agree").creation(new Date().getTime()).updated(new Date(new Date().getTime() + 10000).getTime()).build();
    final CommentDto commentTo1Dto = CommentDto.builder().content("+1").responseTo(comment1Dto).build();
    final CommentDto commentTo1_2Dto = CommentDto.builder().content("correct!").responseTo(comment1Dto).build();




    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test mapping object to dto")
    void toCommentDto() {
        comment1.setComments(List.of(commentTo1, commentTo1_2));
        comment1Dto.setComments(List.of(commentTo1Dto, commentTo1_2Dto));

        comment1.setUpVotes(upvotes);
        comment1.setDownVotes(downvotes);

        comment1Dto.setUpVotes(upvotes.size());
        comment1Dto.setDownVotes(downvotes.size());
        comment1Dto.setEmotes(List.of("upvote"));


        assertEquals(comment1Dto, commentMapperUnderTest.toCommentDto(comment1, logedUser));
    }

    @Test
    @DisplayName("test mapping request to dto when commenting on a comment")
    void toCommentDto_request_toComment() {
        Mockito.when(commentService.findById(anyLong()))
                .thenReturn(comment1Dto);

        PostCommentRequest request = PostCommentRequest.builder()
                .responseTo(1L)
                .postedOn(null)
                .content(comment1Dto.getContent())
                .build();

        CommentDto expected = CommentDto.builder()
                .id(comment1Dto.getId())
                .content(comment1Dto.getContent())
                .user(comment1Dto.getUser())
                .comments(null)
                .build();

        assertEquals(expected, commentMapperUnderTest.toCommentDto(request, posteeDto.getUsername()));
    }
}