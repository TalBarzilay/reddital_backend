package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.controllers.requests.AddSubRequest;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.models.SubReddit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
public class SubRedditMapperTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @InjectMocks
    private SubRedditMapper subRedditMapperUnderTest;

    final String name      = "r/askTal";
    final String category  = "smallTalks";

    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test toSubRedditDto")
    public void toSubRedditDto() {
        // Run the test
        SubReddit post = SubReddit.builder()
                .name(name)
                .category(Category.builder().name(category).build())

                .build();

        final SubRedditDto result = subRedditMapperUnderTest.toSubRedditDto(post);

        assertEquals(name, result.getName());
        assertEquals(category, result.getCategory());
    }

    @Test
    @DisplayName("test toSubRedditDto")
    public void toSubRedditDto_requesr() {
        // Run the test
        AddSubRequest request = AddSubRequest.builder()
                .name(name)
                .category(category)
                .build();

        final SubRedditDto result = subRedditMapperUnderTest.toSubRedditDto(request);

        assertEquals(category, result.getCategory());
    }

    @Test
    @DisplayName("test toSubRedditDto")
    public void toSubRedditDto_null() {
        assertNull(subRedditMapperUnderTest.toSubRedditDto((AddSubRequest) null));
        assertNull(subRedditMapperUnderTest.toSubRedditDto((SubReddit) null));
    }
}
