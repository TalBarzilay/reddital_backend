package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.models.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
public class CategoryMapperTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @MockBean
    private SubRedditMapper subMapper;

    @InjectMocks
    private CategoryMapper categoryMapperUnderTest;

    final String name      = "hobbies";

    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test toCategoryDto")
    public void toCategoryDto() {
        // Run the test
        Category post = Category.builder()
                .name(name)
                .build();

        final CategoryDto result = categoryMapperUnderTest.toCategoryDto(post);

        assertEquals(name, result.getName());
    }

    @Test
    @DisplayName("test toCategoryDto")
    public void toCategoryDto_request() {
        final CategoryDto result = categoryMapperUnderTest.toCategoryDto(name);
        assertEquals(name, result.getName());
    }

    @Test
    @DisplayName("test toCategoryDto")
    public void toCategoryDto_null() {
        assertNull(categoryMapperUnderTest.toCategoryDto((Category) null));
    }
}
