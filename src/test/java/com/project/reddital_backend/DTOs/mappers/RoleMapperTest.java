package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.RoleDto;
import com.project.reddital_backend.models.Role;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RoleMapperTest {
    // ------------------------------------------------------- properties -------------------------------------------------------

    @InjectMocks
    private RoleMapper roleMapperUnderTest;

    final String name      = "BANANA_KING";


    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test toRoleDto")
    public void toRoleDto() {
        // Run the test
        Role role = Role.builder()
                .role(name)
                .build();

        final RoleDto result = roleMapperUnderTest.toRoleDto(role);

        assertEquals(name, result.getRole());
    }

    @Test
    @DisplayName("test toAuthority")
    public void toAuthority(){

        Role role = Role.builder()
                .role(name)
                .build();

        final GrantedAuthority result = roleMapperUnderTest.toAuthority(role);

        assertEquals(name, result.getAuthority());
    }

}

