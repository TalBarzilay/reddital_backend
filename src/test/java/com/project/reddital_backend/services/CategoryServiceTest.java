package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.repositories.CategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(SpringExtension.class)
class CategoryServiceTest {

    // ------------------------------------------------ properties ------------------------------------------------

    @MockBean
    private CategoryRepository mockCategoryRepository;

    @MockBean
    private CategoryMapper catMapper;

    @SpyBean
    private CategoryService catServiceUnderTest;



    private Category cat;
    private CategoryDto cdto;

    // ------------------------------------------------ preperations ------------------------------------------------

    @BeforeEach
    void setUp() {
        cat = Category.builder()
                .name("gaming")
                .build();

        cdto = CategoryDto.builder()
                .name(cat.getName())
                .build();
    }

    @AfterEach
    void tearDown() {
        cat = null;
        cdto = null;
    }



    // ------------------------------------------------ tests ------------------------------------------------



    @Test
    void findByName() {
        Mockito.when(mockCategoryRepository.findByName(anyString()))
                .thenReturn(cat);

        Mockito.when(catMapper.toCategoryDto((Category) any()))
                .thenReturn(cdto);

        assertEquals(cdto, catServiceUnderTest.findByName(cat.getName()));
    }

    @Test
    void findByName_notFound() {
        Mockito.when(mockCategoryRepository.findByName(anyString()))
                .thenThrow(EntityNotFoundException.class);


        assertThrows(EntityNotFoundException.class,() -> catServiceUnderTest.findByName(cat.getName()));
    }




    @Test
    @DisplayName("test create with existing subreddit")
    public void create_subExist() {
        Mockito.when(mockCategoryRepository.findByName(anyString()))
                .thenReturn(cat);

        Mockito.when(catMapper.toCategoryDto((Category) any()))
                .thenReturn(cdto);

        assertThrows(DuplicateEntityException.class, () -> {
            catServiceUnderTest.create(cdto.getName());
        });
    }

    @Test
    @DisplayName("test create with a new category")
    public void create_newCat() {
        //change to mocking to fut the signup method
        Mockito.when(mockCategoryRepository.findByName(anyString()))
                .thenReturn(null);

        // return the user that was received
        Mockito.when(mockCategoryRepository.save(any())).thenAnswer((Answer<Category>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Category) args[0];
        });

        Mockito.when(catMapper.toCategoryDto((Category) any()))
                .thenReturn(cdto);


        // Run the test
        final CategoryDto result = catServiceUnderTest.create(cdto.getName());

        // Verify the results
        assertEquals(cat.getName(), result.getName());
    }

    @Test
    @DisplayName("test findAll categories")
    public void findAll() {

        Category cat2 = Category.builder().name("students").build();
        CategoryDto cdto2 = CategoryDto.builder().name(cat2.getName()).build();

        List<Category>  cats = List.of(cat, cat2);
        List<CategoryDto>  catsdtos = List.of(cdto, cdto2);

        //change to mocking to fut the signup method
        Mockito.when(mockCategoryRepository.findAll())
                .thenReturn(cats);

        // return the user that was received
        Mockito.when(mockCategoryRepository.save(any())).thenAnswer((Answer<Category>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Category) args[0];
        });

        Mockito.when(catMapper.toCategoryDto(cat))
                .thenReturn(cdto);

        Mockito.when(catMapper.toCategoryDto(cat2))
                .thenReturn(cdto2);


        // Run the test
        final List<CategoryDto> result = catServiceUnderTest.getAll();

        // Verify the results
        assertEquals(catsdtos, result);
    }
}
