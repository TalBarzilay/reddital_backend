package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.RoleMapper;
import com.project.reddital_backend.DTOs.models.RoleDto;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.models.Role;
import com.project.reddital_backend.repositories.RoleRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


@ExtendWith(SpringExtension.class)
public class RoleServiceTest {

    // ------------------------------------------------ properties ------------------------------------------------

    @MockBean
    private RoleRepository mockRoleRepository;

    @MockBean
    private RoleMapper mockRoleMapper;

    @SpyBean
    private RoleService roleServiceUnderTest;


    private Role role;
    private RoleDto roleDto;

    final private String name = "JESSES";


    // ------------------------------------------------ preperations ------------------------------------------------

    @BeforeEach
    void setUp() {
        role    = Role.builder().role(name).build();
        roleDto = RoleDto.builder().role(role.getRole()).build();
    }

    @AfterEach
    void tearDown() {
        role    = null;
        roleDto = null;
    }


    // ------------------------------------------------ tests ------------------------------------------------



    @Test
    @DisplayName("test findByRole with existing one")
    void findByRole() {
        Mockito.when(mockRoleRepository.findByRole(anyString()))
                .thenReturn(role);

        Mockito.when(mockRoleMapper.toRoleDto(any()))
                .thenReturn(roleDto);

        assertEquals(roleDto, roleServiceUnderTest.findByRole(role.getRole()));
    }

    @Test
    @DisplayName("test findByRole with non-existing one")
    void findByRole_notExist() {
        Mockito.when(mockRoleRepository.findByRole(anyString()))
                .thenThrow(EntityNotFoundException.class);


        assertThrows(EntityNotFoundException.class,() -> roleServiceUnderTest.findByRole(role.getRole()));
    }
}
