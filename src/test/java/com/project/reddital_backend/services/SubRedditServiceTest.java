package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.models.SubReddit;
import com.project.reddital_backend.repositories.CategoryRepository;
import com.project.reddital_backend.repositories.SubRedditRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
class SubRedditServiceTest {

    // ------------------------------------------------ properties ------------------------------------------------

    @MockBean
    private SubRedditRepository mockSubRedditRepository;

    @MockBean
    private CategoryRepository mockCategoryRepository;

    @MockBean
    private SubRedditMapper subMapper;

    @MockBean
    private CategoryMapper catMapper;

    @SpyBean
    private SubRedditService subServiceUnderTest;



    private SubReddit sub;
    private Category cat;
    private CategoryDto cdto;
    private SubRedditDto sdto;

    // ------------------------------------------------ preperations ------------------------------------------------

    @BeforeEach
    void setUp() {
        cat = Category.builder().name("smallTalks").build();
        cdto = CategoryDto.builder().name(cat.getName()).build();

        sub = SubReddit.builder()
                .name("r/askTal")
                .category(cat)
                .build();

        sdto = SubRedditDto.builder()
                .name(sub.getName())
                .category(sub.getCategory().getName())
                .build();
    }

    @AfterEach
    void tearDown() {
        sub = null;
        sdto = null;
    }



    // ------------------------------------------------ tests ------------------------------------------------



    @Test
    void findByName() {
        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(sub);

        Mockito.when(subMapper.toSubRedditDto((SubReddit) any()))
                .thenReturn(sdto);

        assertEquals(sdto, subServiceUnderTest.findByName(sub.getName()));
    }

    @Test
    void findByName_notFound() {
        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenThrow(EntityNotFoundException.class);


        assertThrows(EntityNotFoundException.class,() -> subServiceUnderTest.findByName(sub.getName()));
    }




    @Test
    @DisplayName("test create with existing subreddit")
    public void create_subExist() {
        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(sub);

        Mockito.when(subMapper.toSubRedditDto((SubReddit) any()))
                .thenReturn(sdto);

        Mockito.when(mockCategoryRepository.findByName(any()))
                        .thenReturn(cat);

        assertThrows(DuplicateEntityException.class, () -> {
            subServiceUnderTest.create(sdto);
        });
    }

    @Test
    @DisplayName("test create with a non existing category")
    public void create_noCategory() {
        //change to mocking to fut the signup method
        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(null);

        Mockito.when(mockCategoryRepository.findByName(any()))
                .thenReturn(null);


        Mockito.when(subMapper.toSubRedditDto((SubReddit) any()))
                .thenReturn(sdto);


        assertThrows(EntityNotFoundException.class, () -> {
            subServiceUnderTest.create(sdto);
        });
    }

    @Test
    @DisplayName("test findByCategory with a non existing category")
    public void findByCategory_noCategory() {
        Mockito.when(mockCategoryRepository.findByName(any()))
                .thenReturn(null);


        Mockito.when(subMapper.toSubRedditDto((SubReddit) any()))
                .thenReturn(sdto);


        assertThrows(EntityNotFoundException.class, () -> {
            subServiceUnderTest.create(sdto);
        });
    }

    @Test
    @DisplayName("test findByCategory with an existing category")
    public void findByCategory_goodCategory() {
        SubReddit sub2 = SubReddit.builder().name("r/askReddit").category(cat).build();
        SubRedditDto sdto2 = SubRedditDto.builder().name(sub2.getName()).category(sub2.getCategory().getName()).build();

        List<SubReddit> subs = List.of(sub, sub2);
        List<SubRedditDto> sdtos = List.of(sdto, sdto2);

        Mockito.when(mockCategoryRepository.findByName(any()))
                .thenReturn(cat);

        Mockito.when(mockSubRedditRepository.findByCategory(cat))
                .thenReturn(subs);


        Mockito.when(subMapper.toSubRedditDto(sub))
                .thenReturn(sdto);

        Mockito.when(subMapper.toSubRedditDto(sub2))
                .thenReturn(sdto2);


        List<SubRedditDto> result = subServiceUnderTest.findByCategory(cdto);

        assertEquals(sdtos, result);

    }
}