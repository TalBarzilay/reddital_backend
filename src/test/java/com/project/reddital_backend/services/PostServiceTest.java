package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.PostMapper;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.exceptions.BadParametersException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.exceptions.NotAllowedException;
import com.project.reddital_backend.exceptions.UnauthorizedException;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.SubReddit;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.PostRepository;
import com.project.reddital_backend.repositories.SubRedditRepository;
import com.project.reddital_backend.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(SpringExtension.class)
class PostServiceTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @MockBean
    private PostRepository mockPostRepository;

    @MockBean
    private UserRepository mockUserRepository;

    @MockBean
    private SubRedditRepository mockSubRedditRepository;

    @MockBean
    private PostMapper postMapper;


    @SpyBean
    private PostService postServiceUnderTest;


    private final List<String> upvotes    = List.of("tal", "tal42", "yosi");
    private final List<String> downvotes  = List.of("mrBanana", "theCat", "theInteger42");

    private User user;
    private SubReddit sub;
    private SubRedditDto subDto;
    private Post post;
    private PostDto postDto;

    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {

        user = User.builder().username("tal").id(157).build();
        sub = SubReddit.builder().name("r/askTal").id(39).build();
        subDto = SubRedditDto.builder().name(sub.getName()).id(sub.getId()).build();

        post = Post.builder()
                .id(42)
                .title("hello evryone")
                .content("this is YOUR dailly dose of tal")
                .creation(new Date())
                .lastUpdated(new Date())
                .subreddit(sub)
                .user(user)
                .upVotes(upvotes.stream().map(username -> User.builder().username(username).build()).collect(Collectors.toList()))
                .downVotes(downvotes.stream().map(username -> User.builder().username(username).build()).collect(Collectors.toList()))
                .build();

        postDto = PostDto.builder()
                .title(post.getTitle())
                .content(post.getContent())
                .subReddit(post.getSubreddit().getName())
                .username(post.getUser().getUsername())
                .creation(post.getCreation().getTime())
                .lastUpdated(post.getLastUpdated().getTime())
                .upVotes(post.getUpVotes().size())
                .downVotes(post.getDownVotes().size())
                .build();
    }

    @AfterEach
    public void tearDown() {
        post = null;
        postDto = null;
    }


    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test posting a post with good values")
    public void posting_good() {

        Mockito.when(mockPostRepository.save(any()))
                .thenReturn(post);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(sub);

        Mockito.when(postMapper.toPostDto(any(), anyString(), anyString()))
                .thenReturn(postDto);

        Mockito.when(postMapper.toPostDto(any(), eq(null)))
                .thenReturn(postDto);



        PostDto result = postServiceUnderTest.posting(postDto);

        // Verify the results
        assertEquals(postDto.getTitle(), result.getTitle());
        assertEquals(postDto.getContent(), result.getContent());
        assertEquals(postDto.getSubReddit(), result.getSubReddit());
        assertTrue(result.getId() >= 0);
        assertTrue(Math.abs(new Date().getTime() - result.getCreation()) < 1000);
    }

    @Test
    @DisplayName("test posting a post with with a bad user")
    public void posting_badUser() {

        Mockito.when(mockPostRepository.save(any()))
                .thenReturn(post);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(null);

        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(sub);

        Mockito.when(postMapper.toPostDto(any(), anyString(), anyString()))
                .thenReturn(postDto);


        // should throw an error
        assertThrows(UnauthorizedException.class, () -> postServiceUnderTest.posting(postDto));
    }

    @Test
    @DisplayName("test posting a post with with a bad subreddit")
    public void posting_badsubReddit() {

        Mockito.when(mockPostRepository.save(any()))
                .thenReturn(post);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(null);

        Mockito.when(postMapper.toPostDto(any(), anyString(), anyString()))
                .thenReturn(postDto);


        // should throw an error
        assertThrows(BadParametersException.class, () -> postServiceUnderTest.posting(postDto));
    }














    @Test
    @DisplayName("test find post of subreddit with bad subreddit")
    public void postsOfSub_badSubReddit() {

        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(null);

        // should throw an error
        assertThrows(BadParametersException.class, () -> postServiceUnderTest.postsOfSub(subDto, null));
    }


    @Test
    @DisplayName("test find post of subreddit with good subreddit")
    public void postsOfSub_good() {

        Post post2 = Post.builder()
                .id(69)
                .title("help! I am a frog!")
                .content("I think that I an a frog! what should I do???")
                .creation(new Date(new Date().getTime() + 1000))
                .lastUpdated(new Date(new Date().getTime() + 100000))
                .subreddit(sub)
                .user(user)
                .build();

        Post post3 = Post.builder()
                .id(18)
                .title("what should I do if my brother thinks that he is a frog?")
                .content("my brother is CONVINCED that he is a frog,\n he even started to eat flies of the wall! what should I do?")
                .creation(new Date(new Date().getTime() + 250000))
                .lastUpdated(new Date(new Date().getTime() + 350000))
                .subreddit(sub)
                .user(user)
                .build();

        List<Post> postsInSub = List.of(post, post2, post3);
        List<PostDto> postsDtoInSub = List.of(postDto, toDto(post2), toDto(post3));


        Mockito.when(mockSubRedditRepository.findByName(anyString()))
                .thenReturn(sub);

        Mockito.when(mockPostRepository.findBySubredditOrderByLastUpdatedDesc(sub))
                .thenReturn(postsInSub);

        Mockito.when(postMapper.toPostDto(post, null))
                .thenReturn(postDto);

        Mockito.when(postMapper.toPostDto(post2, null))
                .thenReturn(toDto(post2));

        Mockito.when(postMapper.toPostDto(post3, null))
                .thenReturn(toDto(post3));

       assertArrayEquals(postsDtoInSub.toArray(), postServiceUnderTest.postsOfSub(subDto, null).toArray());
    }









    @Test
    @DisplayName("test find by id exist")
    public void findById_Good() {
        Mockito.when(mockPostRepository.findById(any()))
                .thenReturn(Optional.of(post));

        Mockito.when(postMapper.toPostDto(any(), eq(null)))
                .thenReturn(postDto);

        assertEquals(postDto, postServiceUnderTest.findById(post.getId()));
    }

    @Test
    @DisplayName("test find by id exist")
    public void findById_NotExist() {
        Mockito.when(mockPostRepository.findById(any()))
                .thenReturn(Optional.empty());


        assertThrows( EntityNotFoundException.class,() -> postServiceUnderTest.findById(post.getId()));
    }











    @Test
    @DisplayName("test upvotes not been upvoted")
    public void upvote_good() {
        String new_user = "NEW_USER";

       Mockito.when(mockUserRepository.findByUsername(anyString()))
                       .thenReturn(User.builder().username(new_user).build());

       Mockito.when(mockPostRepository.findById(anyLong()))
                       .thenReturn(Optional.ofNullable(post));

       Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


       PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.UpVote);

       assertEquals(upvotes.size() + 1, afterUpVote.getUpVotes());
        assertEquals(downvotes.size(), afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test upvotes user already upvoted the post")
    public void upvote_alreadyUpvoted() {
        String new_user = upvotes.get(0);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.UpVote);

        assertEquals(upvotes.size() - 1, afterUpVote.getUpVotes());
        assertEquals(downvotes.size(), afterUpVote.getDownVotes());
    }


    @Test
    @DisplayName("test downvote not been downvoted")
    public void downvote_good() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.DownVote);

        assertEquals(upvotes.size(), afterUpVote.getUpVotes());
        assertEquals(downvotes.size() + 1, afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test downvotes user already downvoted the post")
    public void downvote_alreadyDownvoted() {
        String new_user = downvotes.get(0);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.DownVote);

        assertEquals(upvotes.size(), afterUpVote.getUpVotes());
        assertEquals(downvotes.size() -1, afterUpVote.getDownVotes());
    }





    @Test
    @DisplayName("test upvote user already downvoted the post")
    public void upvote_alreadyDownvoted() {
        String new_user = downvotes.get(0);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.UpVote);

        assertEquals(upvotes.size() + 1, afterUpVote.getUpVotes()); // no longer upvoting the post
        assertEquals(downvotes.size() - 1, afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test downvote user already upvoted the post")
    public void downvote_alreadyUpvoted() {
        String new_user = upvotes.get(0);

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.DownVote);

        assertEquals(upvotes.size() - 1, afterUpVote.getUpVotes()); // no longer upvoting the post
        assertEquals(downvotes.size() + 1, afterUpVote.getDownVotes());
    }



    @Test
    @DisplayName("test emote user not exist")
    public void emote_noUser() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(null);

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));


        assertThrows(EntityNotFoundException.class, () -> postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.DownVote));
    }

    @Test
    @DisplayName("test emote user not exist")
    public void emote_noPost() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().upVotes(p.getUpVotes().size()).downVotes(p.getDownVotes().size()).build();
        });

        assertThrows(EntityNotFoundException.class, () -> postServiceUnderTest.emote(post.getId(), new_user, PostService.Emotes.DownVote));
    }








    @Test
    @DisplayName("Block existing post")
    public void block_exsist(){
        postDto.setBlocked(false);

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any()))
                .thenReturn(post);

        Mockito.when(postMapper.toPostDto(any(), eq(null)))
                .thenReturn(postDto);

        assertEquals(postDto, postServiceUnderTest.toggleBlock(post.getId()));
    }

    @Test
    @DisplayName("unBlock existing post")
    public void unblock_exsist(){
        postDto.setBlocked(true);

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any()))
                .thenReturn(post);

        Mockito.when(postMapper.toPostDto(any(), eq(null)))
                .thenReturn(postDto);

        assertEquals(postDto, postServiceUnderTest.toggleBlock(post.getId()));
    }


    @Test
    @DisplayName("Block non-existing post")
    public void block_Notexsist(){
        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> postServiceUnderTest.toggleBlock(post.getId()));
    }










    @Test
    @DisplayName("test getting ammount of new post this month")
    public void numOfUsersOfLastMonth(){
        List<Post> postList = List.of(post, post, post);

        Mockito.when(mockPostRepository.findByCreationAfter(any()))
                .thenReturn(postList);

        assertEquals(postList.size(), postServiceUnderTest.numOfPostsOfLastMonth());
    }













    @Test
    @DisplayName("test edditing posts")
    public void edit_post_good() {
        String newContent = "hello! I am the  new content of the post!";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(post.getUser());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().content(p.getContent()).build();
        });


        PostDto afterUpVote = postServiceUnderTest.edit(post.getId(), post.getUser().getUsername(), newContent, null, false);

        assertEquals(newContent, afterUpVote.getContent());
    }



    @Test
    @DisplayName("test edditing post of diffrent user")
    public void edit_post_of_another_user() {
        String newContent = "hello! I am the  new content of the post!";
        String other = "NOT_OWNER";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(other).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().content(p.getContent()).build();
        });


        assertThrows(NotAllowedException.class, () -> postServiceUnderTest.edit(post.getId(), other, newContent,null, false));
    }

    @Test
    @DisplayName("test edditing post of diffrent user when admin")
    public void edit_post_of_another_user_admin() {
        String newContent = "hello! I am the  new content of the post!";
        String other = "NOT_OWNER";

        Mockito.when(mockUserRepository.findByUsername(anyString()))
                .thenReturn(User.builder().username(other).build());

        Mockito.when(mockPostRepository.findById(anyLong()))
                .thenReturn(Optional.ofNullable(post));

        Mockito.when(mockPostRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(postMapper.toPostDto(any(), anyString())).thenAnswer(i -> {
            Post p = (Post) i.getArguments()[0];

            return PostDto.builder().content(p.getContent()).build();
        });

        PostDto afterUpVote = postServiceUnderTest.edit(post.getId(), post.getUser().getUsername(), newContent,null, true);

        assertEquals(newContent, afterUpVote.getContent());
    }




    // ------------------------------------------------ private methods ------------------------------------------------

    private PostDto toDto(Post post){
        if (post == null)
            return null;

        return PostDto.builder()
                .id(post.getId())
                .title(post.getTitle())
                .content(post.getContent())
                .creation(post.getCreation().getTime())
                .lastUpdated(post.getLastUpdated().getTime())
                .subReddit(post.getSubreddit().getName())
                .username(post.getUser().getUsername())
                .build();
    }


}