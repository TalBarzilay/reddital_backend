package com.project.reddital_backend.services;


import com.project.reddital_backend.DTOs.mappers.CommentMapper;
import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.exceptions.*;
import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.CommentRepository;
import com.project.reddital_backend.repositories.PostRepository;
import com.project.reddital_backend.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class CommentServiceTest {

    // ------------------------------------------------ properties ------------------------------------------------

    @MockBean
    private CommentRepository mockCommentRepo;

    @MockBean
    private PostRepository mockPostRepo;

    @MockBean
    private UserRepository mockUserRepo;

    @MockBean
    private CommentMapper commentMapper;


    @SpyBean
    private CommentService commentServiceUnderTest;



    private final List<String> upvotes    = List.of("tal", "tal42", "yosi");
    private final List<String> downvotes  = List.of("mrBanana", "theCat", "theInteger42");

    final User user = User.builder().username("tal").build();

    final PostDto postTo = PostDto.builder().build();
    final Post post = Post.builder().build();

    final Comment responseTo = Comment.builder().postedOn(post).build();
    final CommentDto responseToDto = CommentDto.builder().postedOn(postTo).build();


    final Comment comment = Comment.builder().id(42).user(user).content("agree").postedOn(post).build();
    final Comment commentTo1 = Comment.builder().content("+1").responseTo(comment).build();
    final Comment commentTo1_2 = Comment.builder().content("correct!").responseTo(comment).build();


    final CommentDto commentDto = CommentDto.builder().user(comment.getUser().getUsername()).content(comment.getContent()).build();
    final CommentDto commentTo1Dto = CommentDto.builder().content(commentTo1.getContent()).responseTo(commentDto).build();
    final CommentDto commentTo1_2Dto = CommentDto.builder().content(commentTo1_2.getContent()).responseTo(commentDto).build();

    // ------------------------------------------------ preperations ------------------------------------------------

    @BeforeEach
    void setUp() {
        commentDto.setComments(List.of(commentTo1Dto, commentTo1_2Dto));

        comment.setUpVotes  (upvotes    .stream().map(username -> User.builder().username(username).build()).collect(Collectors.toList()));
        comment.setDownVotes(downvotes  .stream().map(username -> User.builder().username(username).build()).collect(Collectors.toList()));

        commentDto.setUser("LeeroyJenkins");
        post.setBlocked(false);
        postTo.setBlocked(false);
    }

    @AfterEach
    void tearDown() {
        commentDto.setComments(null);
    }

    // ------------------------------------------------ tests ------------------------------------------------

    @Test
    @DisplayName("test postComment on non existing Post")
    void postComment_nonExistsPost() {
        Mockito.when(mockPostRepo.findById(anyLong()))
                .thenReturn(Optional.empty());

        commentDto.setPostedOn(postTo);
        commentDto.setResponseTo(null);

        assertThrows(EntityNotFoundException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }

    @Test
    @DisplayName("test postComment on an existing Post")
    void postComment_goodPost() {
        Mockito.when(mockPostRepo.findById(anyLong()))
                .thenReturn(Optional.of(post));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(commentMapper.toCommentDto(comment, null))
                .thenReturn(commentDto);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(user);

        commentDto.setPostedOn(postTo);
        commentDto.setResponseTo(null);

        CommentDto result = commentServiceUnderTest.postComment(commentDto);

        assertEquals(commentDto, result);
    }

    @Test
    @DisplayName("test postComment on an blocking Post")
    void postComment_goodPost_blocked() {
        post.setBlocked(true);

        Mockito.when(mockPostRepo.findById(anyLong()))
                .thenReturn(Optional.of(post));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(commentMapper.toCommentDto(comment, null))
                .thenReturn(commentDto);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(user);

        commentDto.setPostedOn(postTo);
        commentDto.setResponseTo(null);

        assertThrows(PostBlockedException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }





    @Test
    @DisplayName("test postComment on non existing comment")
    void postComment_nonExistsComment() {
        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.empty());

        commentDto.setPostedOn(null);
        commentDto.setResponseTo(responseToDto);

        assertThrows(EntityNotFoundException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }

    @Test
    @DisplayName("test postComment on an existing comment")
    void postComment_goodComment() {
        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(responseTo));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(commentMapper.toCommentDto(comment, null))
                .thenReturn(commentDto);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(user);

        commentDto.setPostedOn(null);
        commentDto.setResponseTo(responseToDto);

        CommentDto result = commentServiceUnderTest.postComment(commentDto);

        assertEquals(commentDto, result);
    }


    @Test
    @DisplayName("test postComment on an existing comment")
    void postComment_goodComment_blockedpost() {
        post.setBlocked(true);

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(responseTo));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(commentMapper.toCommentDto(comment, null))
                .thenReturn(commentDto);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(user);

        commentDto.setResponseTo(responseToDto);

        assertThrows(PostBlockedException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }







    @Test
    @DisplayName("test postComment when neither post nor comment was specified")
    void postComment_nothingSpecified() {
        commentDto.setPostedOn(null);
        commentDto.setResponseTo(null);

        assertThrows(BadParametersException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }






    @Test
    @DisplayName("test find by id exist")
    public void findById_Good() {
        Mockito.when(mockCommentRepo.findById(any()))
                .thenReturn(Optional.of(comment));

        Mockito.when(commentMapper.toCommentDto((Comment) any(), eq(null)))
                .thenReturn(commentDto);

        assertEquals(commentDto, commentServiceUnderTest.findById(comment.getId()));
    }

    @Test
    @DisplayName("test find by id exist")
    public void findById_NotExist() {
        Mockito.when(mockCommentRepo.findById(any()))
                .thenReturn(Optional.empty());


        assertThrows( EntityNotFoundException.class,() -> commentServiceUnderTest.findById(comment.getId()));
    }

    @Test
    @DisplayName("test find by id exist")
    public void commenting_NoUser() {
        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(responseTo));

        commentDto.setPostedOn(null);
        commentDto.setResponseTo(responseToDto);

        commentDto.setUser(null);

        assertThrows(UnauthorizedException.class, () -> commentServiceUnderTest.postComment(commentDto));
    }





    @Test
    @DisplayName("test postComment on an existing Post, user name not found")
    void postComment_userNotFound_post() {
        Mockito.when(mockPostRepo.findById(anyLong()))
                .thenReturn(Optional.of(post));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                        .thenReturn(null);

        commentDto.setPostedOn(postTo);
        commentDto.setResponseTo(null);

        assertThrows(EntityNotFoundException.class,() -> commentServiceUnderTest.postComment(commentDto));
    }


    @Test
    @DisplayName("test postComment on an existing comment, user name not found")
    void postComment_userNotFound_comment() {
        Mockito.when(mockPostRepo.findById(anyLong()))
                .thenReturn(Optional.of(post));

        Mockito.when(mockCommentRepo.save(any()))
                .thenReturn(comment);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(null);

        commentDto.setPostedOn(null);
        commentDto.setResponseTo(responseToDto);

        assertThrows(EntityNotFoundException.class,() -> commentServiceUnderTest.postComment(commentDto));
    }















    @Test
    @DisplayName("test upvotes not been upvoted")
    public void upvote_good() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.UpVote);

        assertEquals(upvotes.size() + 1, afterUpVote.getUpVotes());
        assertEquals(downvotes.size(), afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test upvotes user already upvoted the post")
    public void upvote_alreadyUpvoted() {
        String new_user = upvotes.get(0);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.UpVote);

        assertEquals(upvotes.size() - 1, afterUpVote.getUpVotes());
        assertEquals(downvotes.size(), afterUpVote.getDownVotes());
    }


    @Test
    @DisplayName("test downvote not been downvoted")
    public void downvote_good() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.DownVote);

        assertEquals(upvotes.size(), afterUpVote.getUpVotes());
        assertEquals(downvotes.size() + 1, afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test downvotes user already downvoted the post")
    public void downvote_alreadyDownvoted() {
        String new_user = downvotes.get(0);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.DownVote);

        assertEquals(upvotes.size(), afterUpVote.getUpVotes());
        assertEquals(downvotes.size() - 1, afterUpVote.getDownVotes());
    }





    @Test
    @DisplayName("test upvote user already downvoted the post")
    public void upvote_alreadyDownvoted() {
        String new_user = downvotes.get(0);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.UpVote);

        assertEquals(upvotes.size() + 1, afterUpVote.getUpVotes()); // no longer upvoting the post
        assertEquals(downvotes.size() - 1, afterUpVote.getDownVotes());
    }

    @Test
    @DisplayName("test downvote user already upvoted the post")
    public void downvote_alreadyUpvoted() {
        String new_user = upvotes.get(0);

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().upVotes(c.getUpVotes().size()).downVotes(c.getDownVotes().size()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.DownVote);

        assertEquals(upvotes.size() - 1, afterUpVote.getUpVotes()); // no longer upvoting the post
        assertEquals(downvotes.size() + 1, afterUpVote.getDownVotes());
    }



    @Test
    @DisplayName("test emote user not exist")
    public void emote_noUser() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(null);

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.ofNullable(comment));


        assertThrows(EntityNotFoundException.class, () -> commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.DownVote));
    }

    @Test
    @DisplayName("test emote user not exist")
    public void emote_noPost() {
        String new_user = "NEW_USER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(new_user).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.empty());


        assertThrows(EntityNotFoundException.class, () -> commentServiceUnderTest.emote(comment.getId(), new_user, CommentService.Emotes.DownVote));
    }













    @Test
    @DisplayName("test eddit comment content by owner")
    public void edit_good() {
        String newContnent = "this is the new content";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(comment.getUser());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().content(c.getContent()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.edit(comment.getId(), comment.getUser().getUsername(), newContnent, false);

        assertEquals(newContnent, afterUpVote.getContent());
    }




    @Test
    @DisplayName("test eddit comment content not by owner")
    public void edit_comment_of_other_user() {
        String newContnent = "this is the new content";
        String otherUser = "NOT_OWNER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(otherUser).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().content(c.getContent()).build();
        });


        CommentDto afterUpVote = commentServiceUnderTest.edit(comment.getId(), comment.getUser().getUsername(), newContnent, true);

        assertEquals(newContnent, afterUpVote.getContent());
    }


    @Test
    @DisplayName("test eddit comment content not by owner when admin")
    public void edit_comment_of_other_user_admin() {
        String newContnent = "this is the new content";
        String otherUser = "NOT_OWNER";

        Mockito.when(mockUserRepo.findByUsername(anyString()))
                .thenReturn(User.builder().username(otherUser).build());

        Mockito.when(mockCommentRepo.findById(anyLong()))
                .thenReturn(Optional.of(comment));

        Mockito.when(mockCommentRepo.save(any())).thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(commentMapper.toCommentDto((Comment) any(), anyString())).thenAnswer(i -> {
            Comment c = (Comment) i.getArguments()[0];

            return CommentDto.builder().content(c.getContent()).build();
        });


        assertThrows(NotAllowedException.class, () -> commentServiceUnderTest.edit(comment.getId(), otherUser, newContnent, false));
    }

}
