package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.RoleMapper;
import com.project.reddital_backend.models.Role;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
class CustomUserDetailsServiceTest {

    // ------------------------------------------------ properties ------------------------------------------------

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleMapper roleMapper;

    @SpyBean
    private  CustomUserDetailsService customUserDetailsServiceUnderTest;


    private User user;


    // ------------------------------------------------ preperations ------------------------------------------------

    @BeforeEach
    void setUp() {
        user = User.builder().username("tal").email("tal@gmail.com").password("123456").roles(List.of()).build();
    }

    @AfterEach
    void tearDown() {
        user = null;
    }

    // ------------------------------------------------ tests ------------------------------------------------

    @Test
    @DisplayName("test loadUserByUsername exist")
    void loadUserByUsername() {
        Mockito.when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(user);

        Mockito.when(roleMapper.toAuthority(any()))
                .thenReturn(new SimpleGrantedAuthority("USER"));

        final UserDetails userDetails = customUserDetailsServiceUnderTest.loadUserByUsername(user.getUsername());

        assertEquals(user.getUsername(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
    }

    @Test
    @DisplayName("test loadUserByUsername not exist")
    void loadUserByUsername_notExist() {
        Mockito.when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(null);

        assertThrows(UsernameNotFoundException.class, () -> customUserDetailsServiceUnderTest.loadUserByUsername(user.getUsername()));
    }
}