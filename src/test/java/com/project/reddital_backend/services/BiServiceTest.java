package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.models.BiDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class BiServiceTest {

    @MockBean
    private UserService userService;

    @MockBean
    private PostService postService;

    @MockBean
    private SubRedditService subRedditService;

    @SpyBean
    private BiService biService;


    @Test
    void bi() {
        int newUsers = 42;
        int newPosts = 69;
        Map<String, Integer> postsOfSub = new HashMap<>();

        Mockito.when(userService.numOfUsersOfLastMonth())
                        .thenReturn(newUsers);

        Mockito.when(postService.numOfPostsOfLastMonth())
                .thenReturn(newPosts);

        Mockito.when(subRedditService.newPostsInSubLastMonth())
                .thenReturn(postsOfSub);

        BiDto expected = BiDto.builder()
                .newUsers(newUsers)
                .newPosts(newPosts)
                .postsOfSub(postsOfSub)
                .build();

        assertEquals(expected, biService.bi());
    }
}