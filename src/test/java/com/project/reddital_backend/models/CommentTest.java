package com.project.reddital_backend.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CommentTest {

    // ---------------------------------------------------- fields ----------------------------------------------------

    final String content = "I don't really agree with you, you are wrong";

    final Date cration = new Date();
    final Date updated = new Date(new Date().getTime() + 100000);
    final User user = User.builder().build();


    final Post postedOn = Post.builder().build();

    final  Comment responseTo = null;



    // ---------------------------------------------------- tests ----------------------------------------------------

    @Test
    @DisplayName("test creating new comment")
    public void buildComment() {
        Comment comment = Comment.builder()
                .content(content)
                .postedOn(postedOn)
                .responseTo(responseTo)
                .user(user)
                .creation(cration)
                .updated(updated)
                .build();

        assertEquals(content,comment.getContent());
        assertEquals(postedOn,comment.getPostedOn());
        assertEquals(responseTo,comment.getResponseTo());
        assertEquals(user,comment.getUser());

        assertEquals(cration,comment.getCreation());
        assertEquals(updated,comment.getUpdated());

        assertEquals(List.of(), comment.getUpVotes());
        assertEquals(List.of(), comment.getDownVotes());
    }
}
