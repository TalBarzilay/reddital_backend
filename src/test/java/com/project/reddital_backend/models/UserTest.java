package com.project.reddital_backend.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
public class UserTest {

    // ------------------------------------------------------- properties -------------------------------------------------------
    final String username   = "Sportalcraft";
    final String email      = "test@test.com";
    final String password   = "123456";

    final Role role1 = Role.builder().role("BANANA_USER").build();
    final Role role2 = Role.builder().role("YOSI_USER").build();
    final List<Role> roles = List.of(role1, role2);

    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test creating new user")
    public void buildUser() {

        User user = User.builder()
                .username(username)
                .email(email)
                .password(password)
                .roles(roles)
                .build();

        assertEquals(username, user.getUsername());
        assertEquals(email, user.getEmail());
        assertEquals(password, user.getPassword());
        assertEquals(roles, user.getRoles());
        assertFalse(user.isBlocked());
    }
}
