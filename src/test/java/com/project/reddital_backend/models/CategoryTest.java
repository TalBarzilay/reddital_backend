package com.project.reddital_backend.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CategoryTest {
    // ------------------------------------------------------- properties -------------------------------------------------------

    final String name      = "help";


    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test creating new category")
    public void buildUser() {

        Category cat = Category.builder().name(name).build();

        assertEquals(name, cat.getName());
    }
}
