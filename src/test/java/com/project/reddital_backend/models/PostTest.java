package com.project.reddital_backend.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
public class PostTest {

    // ------------------------------------------------------- properties -------------------------------------------------------

    final String title      = "i'm new here!";
    final String content    = "hello! just registered!";
    final Date time         = new Date();

    final User user = User.builder().username("a").password("123456").email("a@a.a").build();
    final SubReddit sub = SubReddit.builder().name("r/askTal").build();

    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test creating new user")
    public void buildUser() {

        Post post = Post.builder()
                .title(title)
                .content(content)
                .creation(time)
                .lastUpdated(time)
                .user(user)
                .subreddit(sub)
                .build();

        assertEquals(title, post.getTitle());
        assertEquals(content, post.getContent());
        assertEquals(time, post.getCreation());
        assertEquals(time, post.getLastUpdated());
        assertEquals(user, post.getUser());
        assertEquals(sub, post.getSubreddit());
        assertEquals(List.of(), post.getUpVotes());
        assertEquals(List.of(), post.getDownVotes());
        assertFalse(post.isBlocked());
    }
}
