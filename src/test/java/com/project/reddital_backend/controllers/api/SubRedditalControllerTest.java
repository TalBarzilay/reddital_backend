package com.project.reddital_backend.controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.controllers.requests.AddSubRequest;
import com.project.reddital_backend.controllers.requests.Request;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.security.providers.SecretProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CategoryService;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.SubRedditService;
import com.project.reddital_backend.services.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SubRedditalController.class)
class SubRedditalControllerTest {


    @TestConfiguration
    static class config{

        @Bean
        public FilterProvider filterProvider(){
            return new FilterProvider();
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new TokenProvider();
        }

        @Bean
        public AlgorithmProvider algorithmProvider(){
            return new AlgorithmProvider();
        }

        @Bean
        public  ObjectMapper objectMapper(){
            return new ObjectMapper();
        }

    }

    // ----------------------------------------------------- fields -----------------------------------------------------

    @MockBean
    private SubRedditMapper mockSubMapper;

    @MockBean
    private SubRedditService mockSubService;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private SecretProvider secretProvider;

    @MockBean
    private UserService userService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private CommandLineRunner commandLineRunner;


    @Autowired
    private TokenProvider tokenProvider;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;



    private final org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User("user", "123456", List.of(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN")));

    private ObjectMapper objectMapper;

    private final String SUB_PATH  = "/subredditals";

    final String name = "askTal";
    final String cat = "smallTalks";

    private SubRedditDto sdto;

    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        sdto = SubRedditDto.builder()
                .name(name)
                .build();
    }

    @AfterEach
    public void tearDown() {
        objectMapper = null;
        sdto = null;
    }

    // ----------------------------------------------------- tests -----------------------------------------------------


    @Test
    @DisplayName("test create sub")
    public void create_good() throws Exception {
        Mockito.when(mockSubService.create(any()))
                .thenReturn(sdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Request request = AddSubRequest.builder()
                .name(name)
                .category(cat)
                .build();

        ResultActions result = post(SUB_PATH + "/create", requestAsString(request));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());

        checkProperty(name, "$.name", body);
    }

    @Test
    @DisplayName("test create sub that exist")
    public void create_exist() throws Exception {
        Mockito.when(mockSubService.create(any()))
                .thenThrow(DuplicateEntityException.class);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = AddSubRequest.builder()
                .name(name)
                .category(cat)
                .build();


        ResultActions result = post(SUB_PATH + "/create", requestAsString(request));

        result.andExpect(status().isUnprocessableEntity());
    }





    @Test
    @DisplayName("test delete sub")
    public void delete() throws Exception {
        Mockito.doNothing().when(mockSubService).remove(any());

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);


        ResultActions result = post(SUB_PATH + "/delete", sdto.getName());

        result.andExpect(status().isNoContent());
    }


    // ----------------------------------------------------- private methods -----------------------------------------------------

    private void checkProperty(String expected, String jsonPath, String body){
        assertEquals(expected, "" + JsonPath.read(body, jsonPath));
    }

    private ResultActions post(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"))
        );
    }

    private ResultActions get(String uri) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
        );
    }


    private String requestAsString(Request request) throws JsonProcessingException {
        return objectMapper.writeValueAsString(request);
    }
}