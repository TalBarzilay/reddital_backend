package com.project.reddital_backend.controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.controllers.requests.Request;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.security.providers.SecretProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CategoryService;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CategoryController.class)
class CategoryControllerTest {

    @TestConfiguration
    static class config{

        @Bean
        public FilterProvider filterProvider(){
            return new FilterProvider();
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new TokenProvider();
        }

        @Bean
        public AlgorithmProvider algorithmProvider(){
            return new AlgorithmProvider();
        }
    }

    // ----------------------------------------------------- fields -----------------------------------------------------

    @MockBean
    private CategoryMapper mockCatMapper;

    @MockBean
    private CategoryService mockCatService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private SecretProvider secretProvider;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private CommandLineRunner commandLineRunner;

    @Autowired
    private TokenProvider tokenProvider;

    @MockBean
    private UserService userService;





    @Autowired
    private MockMvc mockMvc;



    private final org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User("user", "123456", List.of(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN")));

    private ObjectMapper objectMapper;

    private final String CAT_PATH  = "/categories";

    final String cat = "programing";

    private CategoryDto cdto;

    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        cdto = CategoryDto.builder()
                .name(cat)
                .build();
    }

    @AfterEach
    public void tearDown() {
        objectMapper = null;
        cdto = null;
    }

    // ----------------------------------------------------- tests -----------------------------------------------------


    @Test
    @DisplayName("test create cat")
    public void create_good() throws Exception {
        Mockito.when(mockCatService.create(any()))
                .thenReturn(cdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        ResultActions result = post(CAT_PATH + "/create", cat);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());

        checkProperty(cat, "$.name", body);
    }

    @Test
    @DisplayName("test create cat that exist")
    public void create_exist() throws Exception {
        Mockito.when(mockCatService.create(any()))
                .thenThrow(DuplicateEntityException.class);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        ResultActions result = post(CAT_PATH + "/create", cat);

        result.andExpect(status().isUnprocessableEntity());
    }

    @Test
    @DisplayName("test delete cat")
    public void delete() throws Exception {
        Mockito.doNothing().when(mockCatService).remove(any());

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);


        ResultActions result = post(CAT_PATH + "/delete", cat);
        result.andExpect(status().isNoContent());
    }


    @Test
    @DisplayName("get all categories")
    public void getAll() throws Exception {
        final String cat2 = "gaming";

        CategoryDto cdto1 = CategoryDto.builder().name(cat).build();
        CategoryDto cdto2 = CategoryDto.builder().name(cat2).build();

        SubRedditDto sdto2 = SubRedditDto.builder().name("r/askReddit").category(cat).build();
        SubRedditDto sdto3 = SubRedditDto.builder().name("r/askYosi").category(cat).build();

        SubRedditDto sdto1_gaming = SubRedditDto.builder().name("r/HalfLifeAlyx").category(cat2).build();
        SubRedditDto sdto2_gaming = SubRedditDto.builder().name("r/Terreria").category(cat2).build();
        SubRedditDto sdto3_gaming = SubRedditDto.builder().name("r/Subnautica").category(cat2).build();

        cdto1.setSubredditals(List.of(sdto2, sdto3));
        cdto2.setSubredditals(List.of(sdto1_gaming, sdto2_gaming, sdto3_gaming));

        List<CategoryDto> categories = List.of(cdto1, cdto2);


        Mockito.when(mockCatService.getAll())
                .thenReturn(categories);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        ResultActions result = get(CAT_PATH + "/all");
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        assertEquals(cat,  JsonPath.read(body, "$[0].name"));
        assertEquals("r/askReddit",  JsonPath.read(body, "$[0].subredditals[0].name"));
        assertEquals("r/askYosi",  JsonPath.read(body, "$[0].subredditals[1].name"));

        assertEquals(cat2,  JsonPath.read(body, "$[1].name"));
        assertEquals("r/HalfLifeAlyx",  JsonPath.read(body, "$[1].subredditals[0].name"));
        assertEquals("r/Terreria",  JsonPath.read(body, "$[1].subredditals[1].name"));
        assertEquals("r/Subnautica",  JsonPath.read(body, "$[1].subredditals[2].name"));

    }




    // ----------------------------------------------------- private methods -----------------------------------------------------

    private void checkProperty(String expected, String jsonPath, String body){
        assertEquals(expected, "" + JsonPath.read(body, jsonPath));
    }

    private ResultActions post(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path")
        ));
    }

    private ResultActions get(String uri) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
        );
    }

    private String requestAsString(Request request) throws JsonProcessingException {
        return objectMapper.writeValueAsString(request);
    }
}