package com.project.reddital_backend.controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.project.reddital_backend.DTOs.mappers.CommentMapper;
import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.controllers.requests.EditRequest;
import com.project.reddital_backend.controllers.requests.EmoteRequest;
import com.project.reddital_backend.controllers.requests.PostCommentRequest;
import com.project.reddital_backend.controllers.requests.Request;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.security.providers.SecretProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CommentService;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CommentController.class)
public class CommentControllerTest {

    @TestConfiguration
    static class config{

        @Bean
        public FilterProvider filterProvider(){
            return new FilterProvider();
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new TokenProvider();
        }

        @Bean
        public AlgorithmProvider algorithmProvider(){
            return new AlgorithmProvider();
        }
    }

    // ----------------------------------------------------- fields -----------------------------------------------------

    @MockBean
    private CommentMapper mapper;

    @MockBean
    private CommentService mockCommentService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private SecretProvider secretProvider;

    @Autowired
    private TokenProvider tokenProvider;

    @MockBean
    private UserService userService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private CommandLineRunner commandLineRunner;


    @MockBean
    private UserRepository userRepository;


    @Autowired
    private MockMvc mockMvc;


    final String COMMENT_PATH = "/comments";

    private CommentDto comment;

    private ObjectMapper objectMapper;

    private final org.springframework.security.core.userdetails.User user         = new org.springframework.security.core.userdetails.User("user"         , "123456", List.of(new SimpleGrantedAuthority("USER")));
    private final org.springframework.security.core.userdetails.User user_empty   = new org.springframework.security.core.userdetails.User("user_empty"   , "123456", List.of());


    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        comment = CommentDto.builder()
                .id(42L)
                .content("this is vert wrong!")
                .comments(null)
                .creation(new Date().getTime() - 100000)
                .updated(new Date().getTime())
                .upVotes(42)
                .downVotes(69)
                .build();
    }

    @AfterEach
    public void tearDown() {
        objectMapper = null;
        comment = null;
    }

    // ----------------------------------------------------- tests -----------------------------------------------------

    @Test
    @DisplayName("test post comment on post")
    public void testPostComment_onPost() throws Exception {
        comment.setPostedOn(PostDto.builder().id(54L).build());

        Mockito.when(mockCommentService.postComment(comment))
                .thenReturn(comment);

        Mockito.when(mapper.toCommentDto((PostCommentRequest) any(), anyString()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);


        Request request = PostCommentRequest.builder()
                .content(comment.getContent())
                .postedOn(comment.getPostedOn().getId())
                .build();

        ResultActions result = post(COMMENT_PATH + "/commenting", requestAsString(request), tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());


        checkProperty(comment.getContent(), "$.content", body);
        checkProperty("" + comment.getCreation(), "$.creation", body);
        checkProperty("" + comment.getUpdated(), "$.updated", body);
    }

    @Test
    @DisplayName("test post comment on comment")
    public void testPostComment_onComment() throws Exception {
        comment.setResponseTo(CommentDto.builder().id(54L).build());

        Mockito.when(mockCommentService.postComment(comment))
                .thenReturn(comment);

        Mockito.when(mapper.toCommentDto((PostCommentRequest) any(), anyString()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenReturn(user);

        Request request = PostCommentRequest.builder()
                .content(comment.getContent())
                .responseTo(comment.getResponseTo().getId())
                .build();

        ResultActions result = post(COMMENT_PATH + "/commenting", requestAsString(request), tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());

        checkProperty(comment.getContent(), "$.content", body);
        checkProperty("" + comment.getCreation(), "$.creation", body);
        checkProperty("" + comment.getUpdated(), "$.updated", body);
    }


    @Test
    @DisplayName("test post comment on post no authentication")
    public void testPostComment_onComment_NoAuthentication() throws Exception {
        comment.setPostedOn(PostDto.builder().id(54L).build());

        Mockito.when(mockCommentService.postComment(comment))
                .thenReturn(comment);

        Mockito.when(mapper.toCommentDto((PostCommentRequest) any(), anyString()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Request request = PostCommentRequest.builder()
                .content(comment.getContent())
                .postedOn(comment.getPostedOn().getId())
                .build();

        ResultActions result = post_noAuth(COMMENT_PATH + "/commenting", requestAsString(request));
        result.andExpect(status().isForbidden());
    }



    @Test
    @DisplayName("test post comment on post when user do not have permotions")
    public void testPostComment_onPost_userNoAuthorized() throws Exception {
        comment.setPostedOn(PostDto.builder().id(54L).build());

        Mockito.when(mockCommentService.postComment(comment))
                .thenReturn(comment);

        Mockito.when(mapper.toCommentDto((PostCommentRequest) any(), anyString()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user_empty.getUsername()))
                .thenReturn(user_empty);


        Request request = PostCommentRequest.builder()
                .content(comment.getContent())
                .postedOn(comment.getPostedOn().getId())
                .build();

        ResultActions result = post(COMMENT_PATH + "/commenting", requestAsString(request), tokenProvider.generateAccessToken(user_empty.getUsername(), user_empty.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        result.andExpect(status().isForbidden());
    }












    @Test
    @DisplayName("test emote with good parameters")
    public void emote_good() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockCommentService.emote(anyLong(), anyString(), any()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EmoteRequest.builder()
                .id(comment.getId())
                .emote("upvote")
                .build();

        ResultActions result = post(COMMENT_PATH + "/emote", requestAsString(request), tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty(comment.getContent(), "$.content", body);
        checkProperty( "" + comment.getUpVotes(), "$.upVotes", body);
        checkProperty("" + comment.getDownVotes(), "$.downVotes", body);
    }


    @Test
    @DisplayName("test emote with bad emote")
    public void emote_bad() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockCommentService.emote(anyLong(), anyString(), any()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EmoteRequest.builder()
                .id(comment.getId())
                .emote("banana!")
                .build();

        ResultActions result = post(COMMENT_PATH + "/emote", requestAsString(request), tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        result.andExpect(status().isBadRequest());
    }



    @Test
    @DisplayName("test edit with good parameters")
    public void edit_good() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockCommentService.edit(anyLong(), anyString(), any(), anyBoolean()))
                .thenReturn(comment);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EditRequest.builder()
                .id(comment.getId())
                .content("bananas are great!")
                .build();

        ResultActions result = post(COMMENT_PATH + "/editComment", requestAsString(request), tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty(comment.getContent(), "$.content", body);
        checkProperty( "" + comment.getUpVotes(), "$.upVotes", body);
        checkProperty("" + comment.getDownVotes(), "$.downVotes", body);
    }



    // ----------------------------------------------------- private methods -----------------------------------------------------

    private void checkProperty(String expected, String jsonPath, String body){
        assertEquals(expected, "" + JsonPath.read(body, jsonPath));
    }

    private ResultActions post(String uri, String content, String token) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .header(HttpHeaders.AUTHORIZATION, token)
        );
    }

    private ResultActions post_noAuth(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
        );
    }

    private String requestAsString(Request request) throws JsonProcessingException {
        return objectMapper.writeValueAsString(request);
    }

}
