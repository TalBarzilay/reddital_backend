package com.project.reddital_backend.controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.project.reddital_backend.DTOs.mappers.PostMapper;
import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.controllers.requests.EditRequest;
import com.project.reddital_backend.controllers.requests.EmoteRequest;
import com.project.reddital_backend.controllers.requests.PostingRequest;
import com.project.reddital_backend.controllers.requests.Request;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.exceptions.PostBlockedException;
import com.project.reddital_backend.exceptions.UnauthorizedException;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.security.providers.SecretProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.PostService;
import com.project.reddital_backend.services.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PostController.class)
public class PostControllerTest {

    @TestConfiguration
    static class config{

        @Bean
        public FilterProvider filterProvider(){
            return new FilterProvider();
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new TokenProvider();
        }

        @Bean
        public AlgorithmProvider algorithmProvider(){
            return new AlgorithmProvider();
        }
    }

    // ----------------------------------------------------- fields -----------------------------------------------------

    @MockBean
    private PostMapper mockPostMapper;

    @MockBean
    private SubRedditMapper mockSubMapper;

    @MockBean
    private PostService mockPostService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private SecretProvider secretProvider;

    @Autowired
    private TokenProvider tokenProvider;

    @MockBean
    private UserService userService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private CommandLineRunner commandLineRunner;


    @Autowired
    private MockMvc mockMvc;



    private final org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User("user", "123456", List.of(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority( "ADMIN")));

    private ObjectMapper objectMapper;

    private final String POST_PATH  = "";

    final String title = "i'm tal, AMA";
    final String content = "all questions will be answered!";
    final String subReddit = "r/askTal";
    final String username = "tal";

    private PostDto pdto;


    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        pdto = PostDto.builder()
                .title(title)
                .content(content)
                .subReddit(subReddit)
                .username(username)
                .upVotes(58)
                .downVotes(12)
                .build();
    }

    @AfterEach
    public void tearDown() {
        objectMapper = null;
        pdto = null;
    }

    // ----------------------------------------------------- tests -----------------------------------------------------


    @Test
    @DisplayName("test posting with good parameters")
    public void posting_good() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockPostService.posting(any())).thenAnswer((Answer<PostDto>) invocation -> {
            Object[] args = invocation.getArguments();
            PostDto post =  (PostDto) args[0];
            return post.setCreation(new Date().getTime());
        });

        Mockito.when(mockPostMapper.toPostDto(any(), anyString(), anyString()))
                .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = PostingRequest.builder()
                .title(title)
                .content(content)
                .build();

        ResultActions result = post(POST_PATH + "/posting", itemAsString(request), subReddit);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());

        checkProperty(title, "$.title", body);
        checkProperty(content, "$.content", body);
        checkProperty(subReddit, "$.subReddit", body);

        long timestamp = JsonPath.read(body, "$.creation");

        assertTrue(Math.abs(timestamp - new Date().getTime()) < 1000);
    }


    @Test
    @DisplayName("test posting with user that does not exist")
    public void posting_noKey() throws Exception {
        Mockito.when(mockPostService.posting(any())).thenThrow(UnauthorizedException.class);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = PostingRequest.builder()
                .title(title)
                .content(content)
                .build();

        ResultActions result = post(POST_PATH + "/posting", itemAsString(request), subReddit);
        result.andExpect(status().isUnauthorized());
    }









    @Test
    @DisplayName("test get posts")
    public void postsOfSub() throws Exception {

        PostDto post2 = PostDto.builder()
                .id(69)
                .title("help! I am a frog!")
                .content("I think that I an a frog! what should I do???")
                .creation(new Date(new Date().getTime() + 100000).getTime())
                .lastUpdated(new Date(new Date().getTime() + 100000).getTime())
                .subReddit(subReddit)
                .username(username)
                .build();

        PostDto post3 = PostDto.builder()
                .id(18)
                .title("what should I do if my brother thinks that he is a frog?")
                .content("my brother is CONVINCED that he is a frog,\n he even started to eat flies of the wall! what should I do?")
                .creation(new Date(new Date().getTime() + 250000).getTime())
                .lastUpdated(new Date(new Date().getTime() + 350000).getTime())
                .subReddit(subReddit)
                .username(username)
                .build();

        List<PostDto> postsDtoInSub = List.of(pdto, post2, post3);



        Mockito.when(mockPostService.postsOfSub(any(), eq(null)))
                .thenReturn(postsDtoInSub);

        Mockito.when(mockSubMapper.toSubRedditDto(anyString()))
                .thenReturn(SubRedditDto.builder().name(subReddit).build());

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);



        ResultActions result = get(POST_PATH + "/posts", subReddit);
        result.andExpect(status().isOk());

        String body = result.andReturn().getResponse().getContentAsString();


        assertEquals(itemAsString(postsDtoInSub), body);
    }







    @Test
    @DisplayName("test get post that exist")
    public void getPost_exsist() throws Exception {
        Mockito.when(mockPostService.findById(anyLong()))
                .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        ResultActions result = get(POST_PATH + "/post", pdto.getId());
        result.andExpect(status().isOk());

        String body = result.andReturn().getResponse().getContentAsString();


        assertEquals(body, itemAsString(pdto));
    }

    @Test
    @DisplayName("test get post that not exist")
    public void getPost_Notexsist() throws Exception {
        Mockito.when(mockPostService.findById(anyLong()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        ResultActions result = get(POST_PATH + "/post", pdto.getId());
        result.andExpect(status().isNotFound());
    }













    @Test
    @DisplayName("test emote with good parameters")
    public void emote_good() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockPostService.emote(anyLong(), anyString(), any()))
                        .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EmoteRequest.builder()
                .id(pdto.getId())
                .emote("upvote")
                .build();

        ResultActions result = post(POST_PATH + "/emote", itemAsString(request), subReddit);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty(title, "$.title", body);
        checkProperty(content, "$.content", body);
        checkProperty(subReddit, "$.subReddit", body);
        checkProperty( "" + pdto.getUpVotes(), "$.upVotes", body);
        checkProperty("" + pdto.getDownVotes(), "$.downVotes", body);
    }


    @Test
    @DisplayName("test emote with bad emote")
    public void emote_bad() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockPostService.emote(anyLong(), anyString(), any()))
                .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EmoteRequest.builder()
                .id(pdto.getId())
                .emote("banana!")
                .build();

        ResultActions result = post(POST_PATH + "/emote", itemAsString(request), subReddit);
        result.andExpect(status().isBadRequest());
    }


    @Test
    @DisplayName("test emote on blocked post")
    public void emote_blocked() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockPostService.emote(anyLong(), anyString(), any()))
                .thenThrow(PostBlockedException.class);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EmoteRequest.builder()
                .id(pdto.getId())
                .emote("upvote")
                .build();

        ResultActions result = post(POST_PATH + "/emote", itemAsString(request), subReddit);
        result.andExpect(status().isUnprocessableEntity());
    }







    @Test
    @DisplayName("test blocking post")
    public void block_post() throws Exception {

        pdto.setBlocked(true);

        //mock such that the same input will be returned
        Mockito.when(mockPostService.toggleBlock(anyLong()))
                .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);


        ResultActions result = post(POST_PATH + "/blockPost", pdto.getId());
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty("" +true, "$.blocked", body);
    }









    @Test
    @DisplayName("test eddit post with good parameters")
    public void edit_good() throws Exception {

        //mock such that the same input will be returned
        Mockito.when(mockPostService.edit(anyLong(), anyString(),anyString(), any(), anyBoolean()))
                .thenReturn(pdto);

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(user.getUsername()))
                .thenReturn(user);

        Request request = EditRequest.builder()
                .id(pdto.getId())
                .content("bananas are awosome!")
                .build();

        ResultActions result = post(POST_PATH + "/editPost", itemAsString(request), subReddit);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty(title, "$.title", body);
        checkProperty(content, "$.content", body);
        checkProperty(subReddit, "$.subReddit", body);
        checkProperty( "" + pdto.getUpVotes(), "$.upVotes", body);
        checkProperty("" + pdto.getDownVotes(), "$.downVotes", body);
    }

    // ----------------------------------------------------- private methods -----------------------------------------------------

    private void checkProperty(String expected, String jsonPath, String body){
        assertEquals(expected, "" + JsonPath.read(body, jsonPath));
    }

    private ResultActions post(String uri, String content, String subreddital) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .param("subreddital", subreddital)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path")
        ));
    }


    private ResultActions post(String uri, long id) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(uri)
                .param("id", "" +id)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path")
                ));
    }

    private ResultActions get(String uri, String subreddital) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .param("subreddital", subreddital)
                .contentType(MediaType.APPLICATION_JSON)
        );
    }

    private ResultActions get(String uri, long id) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .get(uri)
                .param("id", "" + id)
                .contentType(MediaType.APPLICATION_JSON)
        );
    }

    private <T>String itemAsString(T request) throws JsonProcessingException {
        return objectMapper.writeValueAsString(request);
    }
}
