package com.project.reddital_backend.controllers.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.project.reddital_backend.DTOs.mappers.UserMapper;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.controllers.requests.ChangePasswordRequest;
import com.project.reddital_backend.controllers.requests.LoginRequest;
import com.project.reddital_backend.controllers.requests.Request;
import com.project.reddital_backend.controllers.requests.SignupRequest;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.security.providers.SecretProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @TestConfiguration
    static class config{

        @Bean
        public FilterProvider filterProvider(){
            return new FilterProvider();
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new TokenProvider();
        }

        @Bean
        public AlgorithmProvider algorithmProvider(){
            return new AlgorithmProvider();
        }
    }

    // ----------------------------------------------------- fields -----------------------------------------------------

    @MockBean
    private UserMapper userMapper;

    @MockBean
    private UserService mockUserService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    private SecretProvider secretProvider;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private CommandLineRunner commandLineRunner;

    @Autowired
    private TokenProvider tokenProvider;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;



    @Autowired
    private MockMvc mockMvc;


    private ObjectMapper objectMapper;

    private final String USER_PATH  = "";

    private final long      id         = 42L;
    private final String    username   = "Sportalcraft";
    private final String    email      = "test@gmail.com";
    private final String    password   = "12345678";

    private final String blocekdUsername = "mrBanana";

    private final UserDto udto = UserDto.builder().id(id).username(username).email(email).password(password).isBlocked(false).build();
    private final org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(username, password, List.of(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN")));



    // ------------------------------------------------------- preparations -------------------------------------------------------

    @BeforeEach
    public void setUp() {
        openMocks(this);

        objectMapper = new ObjectMapper();
    }

    @AfterEach
    public void tearDown() {
        mockUserService = null;
        objectMapper = null;
        udto.setBlocked(false);
    }

    // ----------------------------------------------------- tests -----------------------------------------------------


    @Test
    @DisplayName("test signup with good parameters")
    public void signup_good() throws Exception {
        Mockito.when(mockUserService.signup(any())).thenAnswer((Answer<UserDto>) invocation -> {
            Object[] args = invocation.getArguments();
            return (UserDto) args[0];
        });

        Mockito.when(userMapper.toUserDto((SignupRequest) any()))
                .thenReturn(UserDto.builder().username(username).email(email).build());

        Request request = SignupRequest.builder()
                        .username(username)
                        .email(email)
                        .password(password)
                .build();

        ResultActions result = post(USER_PATH + "/signup", requestAsString(request));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isCreated());

        checkProperty(username, "$.username", body);
        checkProperty(email, "$.email", body);

        try {
            checkProperty("", "$.password", body);
        } catch (PathNotFoundException ignored) {}

    }

    @Test
    @DisplayName("test signup with an existing username")
    public void signup_duplicate() throws Exception {
        Mockito.when(mockUserService.signup(any()))
                .thenThrow(new DuplicateEntityException("EXIST"));

        Request request = SignupRequest.builder()
                .username(username)
                .email(email)
                .password(password)
                .build();

        ResultActions result = post(USER_PATH + "/signup", requestAsString(request));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isUnprocessableEntity());

        checkProperty("EXIST", "$", body);

        checkNonProperty( "$.username", body);
    }

    @Test
    @DisplayName("test signup with missing username")
    public void signup_missingUsername() throws Exception {
        Mockito.when(mockUserService.signup(any())).thenAnswer((Answer<UserDto>) invocation -> {
            Object[] args = invocation.getArguments();
            return (UserDto) args[0];
        });

        ResultActions result = post(USER_PATH + "/signup", String.format("{\"email\": \"%s\", \"password\":\"%s\"}", email, password));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isBadRequest());

        checkNonProperty( "$.username", body);
    }






    @Test
    @DisplayName("test login with good info")
    public void login_good() throws Exception {
        Mockito.when(mockUserService.login(anyString(), anyString())).thenReturn(
                UserDto.builder()
                        .id(id)
                        .username(username)
                        .email(email)
                        .build()
        );

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(username))
                .thenReturn(user);

        Mockito.when(mockUserService.findUserByUsername(anyString()))
                .thenReturn(udto);


        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Mockito.when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenAnswer(i -> i.getArgument(0).equals(i.getArgument(1)));

        Request request = LoginRequest.builder()
                .username(username)
                .password(password)
                .build();

        ResultActions result = post(USER_PATH + "/login", requestAsString(request));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty("" + id, "$.id", body);
        checkProperty(username, "$.username", body);
        checkProperty(email, "$.email", body);

        try {
            checkProperty("", "$.password", body);
        } catch (PathNotFoundException ignored) {}
    }

    @Test
    @DisplayName("test login with bad username")
    public void login_badUserName() throws Exception {
        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenThrow(UsernameNotFoundException.class);

        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Mockito.when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenAnswer(i -> i.getArgument(0).equals(i.getArgument(1)));

        Request request = LoginRequest.builder()
                .username("")
                .password(password)
                .build();

        ResultActions result = post(USER_PATH + "/login", requestAsString(request));

        result.andExpect(status().is4xxClientError());
    }

    @Test
    @DisplayName("test login with wrong credentials")
    public void login_wrongCredentials() throws Exception {
        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(username))
                .thenThrow(UsernameNotFoundException.class);

        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Mockito.when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenAnswer(i -> i.getArgument(0).equals(i.getArgument(1)));



        Request request = LoginRequest.builder()
                .username(username)
                .password(password + "!")
                .build();

        ResultActions result = post(USER_PATH + "/login", requestAsString(request));
        result.andExpect(status().is4xxClientError());
    }

    @Test
    @DisplayName("test login with non existing user")
    public void login_nonExistUser() throws Exception {
        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenThrow(UsernameNotFoundException.class);

        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Mockito.when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenAnswer(i -> i.getArgument(0).equals(i.getArgument(1)));

        Request request = LoginRequest.builder()
                .username("fiovhrouvhrouvlsfn")
                .password(password )
                .build();

        ResultActions result = post(USER_PATH + "/login", requestAsString(request));
        result.andExpect(status().is4xxClientError());
    }


    @Test
    @DisplayName("test login with bloced user")
    public void login_blocked() throws Exception {
        Mockito.when(mockUserService.findUserByUsername(anyString())).thenReturn(
                UserDto.builder()
                        .id(id)
                        .username(username)
                        .email(email)
                        .isBlocked(true)
                        .build()
        );

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(username))
                .thenReturn(user);


        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Mockito.when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenAnswer(i -> i.getArgument(0).equals(i.getArgument(1)));

        Request request = LoginRequest.builder()
                .username(username)
                .password(password)
                .build();

        ResultActions result = post(USER_PATH + "/login", requestAsString(request));

        result.andExpect(status().is4xxClientError());
    }







    @Test
    @DisplayName("test refreshAccessToken with good refresh token")
    public void refreshAccessToken() throws Exception {
        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockUserService.findUserByUsername(anyString()))
                .thenReturn(udto);


        ResultActions result = post_refresh(USER_PATH + "/refreshAccessToken", null);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        assertNotNull(body);
        assertNotEquals("", body);
    }


    @Test
    @DisplayName("test refreshAccessToken with bad refresh token")
    public void refreshAccessToken_bad() throws Exception {
        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockUserService.findUserByUsername(anyString()))
                .thenReturn(udto);


        ResultActions result = post_refresh(USER_PATH + "/refreshAccessToken", "oifjoe");

        result.andExpect(status().isUnauthorized());
    }













    @Test
    @DisplayName("test blocking user ")
    public void block_user() throws Exception {
        UserDto blocked = UserDto.builder().username(blocekdUsername).roles(List.of("USER")).isBlocked(true).build();

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockUserService.findUserByUsername(username))
                .thenReturn(udto);

        Mockito.when(mockUserService.findUserByUsername(blocekdUsername))
                .thenReturn(blocked);

        Mockito.when(mockUserService.toggleBlock(blocekdUsername))
                .thenReturn(blocked);


        ResultActions result = post_block(USER_PATH + "/blockUser", blocekdUsername);
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty("" + true, "$.blocked", body);
    }






    @Test
    @DisplayName("test change with good info")
    public void changePassword_good() throws Exception {
        String newPass = "ekcjpioecme";

        Mockito.when(mockUserService.changePassword(anyString(), anyString(), anyString())).thenReturn(
                UserDto.builder()
                        .id(id)
                        .username(username)
                        .email(email)
                        .password(newPass)
                        .build()
        );

        Mockito.when(secretProvider.getSecret()).thenReturn("secret");

        Mockito.when(customUserDetailsService.loadUserByUsername(username))
                .thenReturn(user);

        Mockito.when(mockUserService.findUserByUsername(anyString()))
                .thenReturn(udto);


        Mockito.when(bCryptPasswordEncoder.encode(anyString()))
                .thenAnswer(i -> i.getArgument(0));

        Request request = ChangePasswordRequest.builder()
                .password(newPass)
                .oldPassword(user.getPassword())
                .build();

        ResultActions result = post_changePass(USER_PATH + "/changePassword", requestAsString(request));
        String body = result.andReturn().getResponse().getContentAsString();

        result.andExpect(status().isOk());

        checkProperty("" + id, "$.id", body);
        checkProperty(username, "$.username", body);
        checkProperty(email, "$.email", body);

        try {
            checkProperty("", "$.password", body);
        } catch (PathNotFoundException ignored) {}
    }


    // ----------------------------------------------------- private methods -----------------------------------------------------

    private void checkProperty(String expected, String jsonPath, String body){
        assertEquals(expected, "" + JsonPath.read(body, jsonPath));
    }

    private void checkNonProperty(String jsonPath, String body){
        try {
            JsonPath.read(body, jsonPath);
        } catch (PathNotFoundException ignored) {
            return;
        }

        Assertions.fail("the property " + jsonPath + " exist in " + body + ", but it shouldn't");
    }



    private ResultActions post(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
        );
    }

    private ResultActions post_changePass(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path"))
                .content(content)
        );
    }


    private ResultActions post_refresh(String uri, String token) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(HttpHeaders.AUTHORIZATION, token == null ? tokenProvider.generateRefreshToken(user.getUsername(), uri) : token)
        );
    }

    private ResultActions post_block(String uri, String username) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .param("username",  username)
                .header(HttpHeaders.AUTHORIZATION, tokenProvider.generateAccessToken(user.getUsername(),  user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), "/path" ))
        );
    }


    private ResultActions get(String uri, String content) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
        );
    }

    private String requestAsString(Request request) throws JsonProcessingException {
        return objectMapper.writeValueAsString(request);
    }

}
