package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class EmoteRequestTest {

    @Test
    @DisplayName("test validate good upvote")
    public void validate_good_up(){
        Request request = EmoteRequest.builder()
                .id(42)
                .emote("upvote")
                .build();

        request.validate();
    }

    @Test
    @DisplayName("test validate good downvote")
    public void validate_good_down(){
        Request request = EmoteRequest.builder()
                .id(42)
                .emote("downvote")
                .build();

        request.validate();
    }

    @Test
    @DisplayName("test validate bad id")
    public void validate_NegativeId(){
        Request request = EmoteRequest.builder()
                .id(-42)
                .emote("upvote")
                .build();

        assertThrows(BadParametersException.class, request::validate);
    }

    @Test
    @DisplayName("test validate no emote")
    public void validate_NoEmote(){
        Request request = EmoteRequest.builder()
                .id(42)
                .emote("")
                .build();

        assertThrows(BadParametersException.class, request::validate);
    }

    @Test
    @DisplayName("test validate null emote")
    public void validate_NullEmote(){
        Request request = EmoteRequest.builder()
                .id(42)
                .emote(null)
                .build();

        assertThrows(BadParametersException.class, request::validate);
    }

    @Test
    @DisplayName("test validate emote not valid")
    public void validate_EmoteNotValid(){
        Request request = EmoteRequest.builder()
                .id(42)
                .emote("makeBananaCake")
                .build();

        assertThrows(BadParametersException.class, request::validate);
    }
}
