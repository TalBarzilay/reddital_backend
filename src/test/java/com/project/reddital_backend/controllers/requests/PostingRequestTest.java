package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class PostingRequestTest {

    @Test
    @DisplayName("test validate good")
    public void validate_good(){
        Request postingRequest = PostingRequest.builder()
                .title("hello world!")
                .content("just came to say hello!")
                .build();

        postingRequest.validate();
    }

    @Test
    @DisplayName("test validate bad title")
    public void validate_title(){

        PostingRequest postingRequest = PostingRequest.builder()
                .title("")
                .content("just came to say hello!")
                .build();

        assertThrows(BadParametersException.class, postingRequest::validate);

        postingRequest.setTitle(null);
        assertThrows(BadParametersException.class, postingRequest::validate);
    }
}
