package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AddSubRequestTest {

    @Test
    @DisplayName("test validate good")
    public void validate_good(){
        Request request = AddSubRequest.builder()
                .name("r/AskTal")
                .category("smallTalks")
                .build();

        request.validate();
    }

    @Test
    @DisplayName("test validate bad name")
    public void validate_name(){

        AddSubRequest request = AddSubRequest.builder()
                .name("")
                .category("smallTalks")
                .build();

        assertThrows(BadParametersException.class, request::validate);

        request.setName(null);
        assertThrows(BadParametersException.class, request::validate);
    }

    @Test
    @DisplayName("test validate bad category")
    public void validate_category(){

        AddSubRequest request = AddSubRequest.builder()
                .name("r/AskTal")
                .category("")
                .build();

        assertThrows(BadParametersException.class, request::validate);

        request.setCategory(null);
        assertThrows(BadParametersException.class, request::validate);
    }
}