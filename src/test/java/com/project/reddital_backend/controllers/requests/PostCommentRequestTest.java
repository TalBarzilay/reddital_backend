package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class PostCommentRequestTest {
    // ------------------------------------------------------- properties -------------------------------------------------------

    // ------------------------------------------------------- preparations -------------------------------------------------------


    // ------------------------------------------------------- tests -------------------------------------------------------

    @Test
    @DisplayName("test validate good on post")
    public void validate_good_post(){
        Request postCommentRequestUnderTest = PostCommentRequest.builder()
                .content("+1")
                .postedOn(42L)
                .responseTo(null)
                .build();

        postCommentRequestUnderTest.validate();
    }

    @Test
    @DisplayName("test validate good on comment")
    public void validate_good_commnet(){
        Request postCommentRequestUnderTest = PostCommentRequest.builder()
                .content("+1")
                .postedOn(null)
                .responseTo(42L)
                .build();

        postCommentRequestUnderTest.validate();
    }

    @Test
    @DisplayName("test validate bad")
    public void validate_bad_NoFather(){
        Request postCommentRequestUnderTest = PostCommentRequest.builder()
                .content("+1")
                .postedOn(null)
                .responseTo(null)
                .build();

        assertThrows(BadParametersException.class, postCommentRequestUnderTest::validate);
    }

    @Test
    @DisplayName("test validate bad")
    public void validate_bad_NoContent(){
        PostCommentRequest postCommentRequestUnderTest = PostCommentRequest.builder()
                .content(null)
                .postedOn(42L)
                .responseTo(null)
                .build();

        assertThrows(BadParametersException.class, postCommentRequestUnderTest::validate);

        postCommentRequestUnderTest.setContent("");
        assertThrows(BadParametersException.class, postCommentRequestUnderTest::validate);
    }
}
