package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.models.SubReddit;
import com.project.reddital_backend.repositories.CategoryRepository;
import com.project.reddital_backend.repositories.SubRedditRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Getter
@Setter
public class SubRedditService {

    @Autowired
    private SubRedditRepository subRedditRepository;

    @Autowired
    private CategoryRepository catRepository;


    @Autowired
    private SubRedditMapper subRedditMapper;

    @Autowired
    private CategoryMapper catMapper;

    /**
     * get a subreddit dto buy its name
     * @param name the name of the subreddit dto
     * @return the corresponding subreddit dto
     */
    public SubRedditDto findByName(String name) {
        return subRedditMapper.toSubRedditDto(subRedditRepository.findByName(name));
    }

    /**
     * subs in a category
     * @param cat the category
     * @return a list of all the subs in that category
     */
    public List<SubRedditDto> findByCategory(CategoryDto cat) {
        Optional<Category> category = getOptional(catRepository.findByName(cat.getName()));

        if(category.isEmpty()) {  // there is no such category, error!
            throw new EntityNotFoundException("no such category exists : " + cat.getName());
        }

        return subRedditRepository.findByCategory(category.get()).stream()
                .map(subRedditMapper::toSubRedditDto)
                .collect(Collectors.toList());
    }

    /**
     * create a new subreddit
     * @param toCreate the subreddit to create
     * @return the created subreddit
     */
    public SubRedditDto create(SubRedditDto toCreate) {
        Optional<Category> cat = getOptional(catRepository.findByName(toCreate.getCategory()));
        Optional<SubReddit> sub = getOptional(getSubRedditRepository().findByName(toCreate.getName()));

        if (cat.isEmpty()){
            throw new EntityNotFoundException("no such category Exist : " + toCreate.getCategory());
        }


        if (sub.isEmpty()) {
            //No such sub exit, create the new sub object and save it.

            SubReddit newSub = SubReddit.builder()
                    .name(toCreate.getName())
                    .category(cat.get())
                    .build();

            return subRedditMapper.toSubRedditDto(subRedditRepository.save(newSub)); // save new sub
        }

        // sub already exist. throw exception.
        throw new DuplicateEntityException("Failed to create new subreddital, already exists : " + toCreate.getName());
    }


    /**
     * delete a subreddital
     * @param toDelete the name of the subreddit to delete
     */
    public void remove(String toDelete) {
        Optional<SubReddit> sub = getOptional(getSubRedditRepository().findByName(toDelete));
        sub.ifPresent(subreddital -> getSubRedditRepository().delete(subreddital));
    }



    public Map<String, Integer> newPostsInSubLastMonth(){
        Date firstOfMonth = Date.from(Timestamp.valueOf(LocalDate.now().atStartOfDay().withDayOfMonth(1)).toInstant());
        Map<String, Integer> map = new HashMap<>();

        List<SubReddit> subs = subRedditRepository.findAll();

        subs.forEach(sub ->{
            int count = sub.getPosts().stream().filter(post -> post.getCreation().compareTo(firstOfMonth) > 0).collect(Collectors.toSet()).size();
            map.put(sub.getName(), count);
        });

        return map;
    }


    // --------------------------------------------------- private methods ---------------------------------------------------


    /**
     * get an optional of a given item
     * @param item the item
     * @return an optional of the item
     */
    private <T> Optional<T> getOptional(T item) {
        return Optional.ofNullable(item);
    }
}
