package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.models.BiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BiService {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @Autowired
    private SubRedditService subRedditService;

    /**
     * @return bi information object
     */
    public BiDto bi() {
        int newUsers = userService.numOfUsersOfLastMonth();
        int newPosts = postService.numOfPostsOfLastMonth();

        Map<String, Integer> postsOfSub = subRedditService.newPostsInSubLastMonth();


        return BiDto.builder()
                .newUsers(newUsers)
                .newPosts(newPosts)
                .postsOfSub(postsOfSub)
                .build();
    }

}
