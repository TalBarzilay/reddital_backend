package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.RoleMapper;
import com.project.reddital_backend.DTOs.models.RoleDto;
import com.project.reddital_backend.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleMapper roleMapper;

    /**
     * get a role dto by its name
     * @param role the name of the role
     * @return the corresponding role dto
     */
    public RoleDto findByRole(String role) {
        return roleMapper.toRoleDto(roleRepository.findByRole(role));
    }
}
