package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.CommentMapper;
import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.exceptions.*;
import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.CommentRepository;
import com.project.reddital_backend.repositories.PostRepository;
import com.project.reddital_backend.repositories.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Getter
@Setter
@Service
public class CommentService {

    public enum Emotes {
        UpVote,
        DownVote
    }

    // ------------------------------------------------ properties ------------------------------------------------

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private PostRepository postRepo;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentMapper commentMapper;


    // ------------------------------------------------ methods ------------------------------------------------

    public CommentDto findById(long id){
        return commentMapper.toCommentDto(getCommentRepo().findById(id).orElseThrow(() -> new EntityNotFoundException("No such comment!")), null);
    }


    /**
     * post a new comment.
     * if meant to be on post       , then "response to"    should be null.
     * if meant to be on comment    , then "posted on"      should be null.
     * @param comment the comment to post
     * @throws EntityNotFoundException if Post\comment not exists
     * @throws BadParametersException if neither post nor comment are specify
     * @return the saved comment
     */
    public CommentDto postComment(CommentDto comment) {

        if(comment.getUser() == null){
            throw new UnauthorizedException("you do not have permotions to post the commant!");
        }

        if(comment.getPostedOn() != null && comment.getResponseTo() == null){
            return postCommentOnPost(comment);
        }

        if(comment.getPostedOn() == null && comment.getResponseTo() != null){
            return postCommentOnComment(comment);
        }

        throw new BadParametersException("neither post nor comment are specified!");
    }


    /**
     * add emotes for a comment
     * if user downvoted, and now tries to upvote, he will be removed from the downvote list, and vice versa
     * @param username the name of the user that adds the emote
     * @param em the emote type
     * @return the comentdto of the comment after the emote
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    public CommentDto emote(long id, String username, CommentService.Emotes em) throws EmoteAlreadyExistException {
        Optional<Comment> comment = getCommentRepo().findById(id);
        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(username));

        if(comment.isEmpty()) {
            throw new EntityNotFoundException("comment was not found");
        }

        if(user.isEmpty()) {
            throw new EntityNotFoundException("user was not found : " + username);
        }

        if(getPostOfComment(comment.get()).isBlocked()){
            throw new PostBlockedException();
        }

        switch (em) {
            case UpVote: return upvote(comment.get(), user.get());
            case DownVote: return downvote(comment.get(), user.get());
        }

        throw new IllegalStateException("This line is here for compilation error");
    }



    /**
     * eddit a comment's content
     * @param id the id of the comment to edit
     * @param username the username of the edditor
     * @param content the new content of the comment
     * @return the dto of the comment after edit
     * @throws UnauthorizedException if trying to edit a comment of different user
     */
    public CommentDto edit(long id, String username, String content, boolean isAdmin) throws UnauthorizedException {
        Optional<Comment> comment = getCommentRepo().findById(id);
        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(username));

        if(comment.isEmpty()) {
            throw new EntityNotFoundException("comment was not found");
        }

        if(user.isEmpty()) {
            throw new EntityNotFoundException("user was not found : " + username);
        }

        if(getPostOfComment(comment.get()).isBlocked()){
            throw new PostBlockedException();
        }

        if(!isAdmin && !comment.get().getUser().equals(user.get())){
            throw new NotAllowedException("cannot edit comment of different user!");
        }

        comment.get().setContent(content);

        return commentMapper.toCommentDto(commentRepo.save(comment.get()), user.get().getUsername());
    }



    // ------------------------------------------------ private methods ------------------------------------------------

    /**
     * post a comment on a post
     * @param comment the comment
     * @throws EntityNotFoundException if Post not exists
     * @return the saved comment
     */
    private CommentDto postCommentOnPost(CommentDto comment) {
        Optional<Post> post = postRepo.findById(comment.getPostedOn().getId());
        User user = userRepository.findByUsername(comment.getUser());

        if(user == null){
            throw new EntityNotFoundException("your user could not be verified!");
        }

        if (post.isEmpty()){
            throw new EntityNotFoundException("the requested post does not exists!");
        }

        if(post.get().isBlocked()){
            throw new PostBlockedException();
        }

        Comment created = Comment.builder()
                .comments(null) // new comment - there are no comments to this one, yet.
                .content(comment.getContent())
                .postedOn(post.get())
                .user(user)
                .responseTo(null) //this post is posted on a post, not a comment
                .build();

        return commentMapper.toCommentDto(commentRepo.save(created), null);
    }

    /**
     * post a comment on a comment
     * @param comment the comment
     * @throws EntityNotFoundException if Comment not exists
     * @return the saved comment
     */
    private CommentDto postCommentOnComment(CommentDto comment) {
        Optional<Comment> responseTo = commentRepo.findById(comment.getResponseTo().getId());
        User user = userRepository.findByUsername(comment.getUser());

        if(user == null){
            throw new EntityNotFoundException("your user could not be verified!");
        }

        if (responseTo.isEmpty()){
            throw new EntityNotFoundException("the requested comment does not exists!");
        }

        if(getPostOfComment(responseTo.get()).isBlocked()){
            throw new PostBlockedException();
        }

        Comment created = Comment.builder()
                .comments(null) // new comment - there are no comments to this one, yet.
                .content(comment.getContent())
                .postedOn(null) // this comment is on a comment, nit a post
                .responseTo(responseTo.get())
                .user(user)
                .build();

        return commentMapper.toCommentDto(commentRepo.save(created), null);
    }




    /**
     * downvote a comment
     * if user upvoted, he will be removed from the upvote list
     * @param comment the comment
     * @param user the user that downvotes
     * @return the post dto after downvoting
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    private CommentDto downvote(Comment comment, User user) throws EmoteAlreadyExistException  {

        if(comment.getDownVotes().contains(user)) { // already downvoted, remove it
            comment.getDownVotes().remove(user);
        } else {
            comment.getUpVotes().remove(user);
            comment.getDownVotes().add(user);
        }

        return commentMapper.toCommentDto(commentRepo.save(comment), user.getUsername());
    }

    /**
     * downvote a comment
     * if user downvoted, and now tries to upvote, he will be removed from the downvote list
     * @param comment the comment
     * @param user the user that upvotes
     * @return the comment dto after upvoting
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    private CommentDto upvote(Comment comment, User user) throws EmoteAlreadyExistException  {

        if(comment.getUpVotes().contains(user)) { // already upvoted, remove it
            comment.getUpVotes().remove(user);
        } else {
            comment.getDownVotes().remove(user);
            comment.getUpVotes().add(user);
        }

        return commentMapper.toCommentDto(commentRepo.save(comment), user.getUsername());
    }


    private Post getPostOfComment(Comment comment){
        if(comment.getPostedOn() != null){
            return comment.getPostedOn();
        }

        return getPostOfComment(comment.getResponseTo());
    }
}
