package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.RoleMapper;
import com.project.reddital_backend.exceptions.BadParametersException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.models.Role;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if(username == null || "".equals(username)){
            throw new BadParametersException("username is empty");
        }

        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(username));

        if (user.isEmpty()) {
            throw new UsernameNotFoundException(String.format("The user %s was not found!", username));
        }

        Collection<GrantedAuthority> autorities = new ArrayList<>();
        user.get().getRoles().forEach(role -> autorities.add(roleMapper.toAuthority(role)));

        return new org.springframework.security.core.userdetails.User(user.get().getUsername(), user.get().getPassword(), autorities);
    }
}
