package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.UserMapper;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.exceptions.BadParametersException;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.exceptions.UnauthorizedException;
import com.project.reddital_backend.models.Role;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Getter
@Setter
public class UserService {

    // ------------------------------------------------------- properties -------------------------------------------------------

    @Autowired
    private UserRepository userRepository; // access DB for user operations

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder; // encrypt and decrypt passwords

    @Autowired
    private UserMapper userMapper; // map obj to dto

    // ------------------------------------------------------- methods -------------------------------------------------------

    /**
     * perform a signup method.
     * @throws DuplicateEntityException if a user with such username already exsists.
     * @param userDto the information on the user to be created
     * @return the user dto that was saved to the DB
     */
    public UserDto signup(UserDto userDto) throws DuplicateEntityException {
        Optional<User> user = getOptional(getUserRepository().findByUsername(userDto.getUsername()));

        if (user.isEmpty()) {
            //No such user exit, create the new user object and save it.

            Role role = getRoleRepository().findByRole("USER");

            if(role == null){
                throw new EntityNotFoundException("NO USER ROLE");
            }

            User newUser = new User()
                    .setUsername(userDto.getUsername())
                    .setEmail(userDto.getEmail())
                    .setPassword(userDto.getPassword())
                    .setRoles(List.of(role));

            return saveUser(newUser, true); // save new user, and encrypt its password.
        }

        // user already exist. throw exception.
        throw new DuplicateEntityException("Failed to signup, user already exists : " + user.get().getUsername());
    }

    /**
     * find and return a user dto by the username
     * @throws EntityNotFoundException if no such user exist
     * @param username the username to search a user by
     * @return the requested user dto
     */
    public UserDto findUserByUsername(String username) throws EntityNotFoundException {
        Optional<User> user = getOptional(getUserRepository().findByUsername(username));

        if (user.isPresent()) {
            return mapDto(user.get()); // return requested user
        }

       // user does not exist
       throw new EntityNotFoundException(String.format("The user %s was not found!", username));
    }

    /**
     * find and return a user dto by the id
     * @throws EntityNotFoundException if no such user exist
     * @param id the id to search a user by
     * @return the requested user dto
     */
    public UserDto findUserById(long id) throws EntityNotFoundException {
        Optional<User> user = getOptional(getUserRepository().findById(id));

        if (user.isPresent()) {
            return mapDto(user.get()); // return requested user
        }

        // user does not exist
        throw new EntityNotFoundException(String.format("The user with id %d was not found!", id));
    }


    /**
     * update a user profile
     * @throws EntityNotFoundException if no such user exist
     * @param userDto the user with the updated profile info
     * @return the updated user dto in the DB
     */
    public UserDto updateProfile(UserDto userDto) throws EntityNotFoundException {
        Optional<User> user = getOptional(getUserRepository().findByUsername(userDto.getUsername()));

        if(!isValidEmailAddress(userDto.getEmail())){
            throw new BadParametersException("email is not valid");
        }

        if (user.isPresent()) {
            User userModel = user.get();

            // update requested user info
            userModel
                    .setEmail(userDto.getEmail());

            // save changes and return them
            return saveUser(userModel, false);
        }

        // user does not exist
        throw new EntityNotFoundException(String.format("The user %s was not found!", userDto.getUsername()));
    }

    /**
     * change a user's password
     * @throws EntityNotFoundException if no such user exist
     * @param username the user to change its the password
     * @param newPassword the new password (plain text)
     * @return the new updated user's dto
     */
    public UserDto changePassword(String username, String newPassword, String oldPass) throws EntityNotFoundException {
        Optional<User> user = getOptional(getUserRepository().findByUsername(username));

        if(newPassword == null || newPassword.length() < 6){
            throw new BadParametersException("new password is not valid");
        }

        if (user.isEmpty()) {
            throw new EntityNotFoundException(String.format("The user %s was not found!", username));
        }

        if(!bCryptPasswordEncoder.matches(oldPass, user.get().getPassword())){
            throw new BadParametersException("old password is inncorrect");
        }

        User userModel = user.get();
        userModel.setPassword(newPassword); // update the new password

        return saveUser(userModel, true); // update the user info, and encrypt the password

    }

    /**
     * perform a login
     * @throws EntityNotFoundException if such user does not exist
     * @throws UnauthorizedException if login failed
     * @param username the username
     * @param password the password of the user
     * @return null if can't log in, the user dto otherwise
     */
    public UserDto login(String username, String password) throws UnauthorizedException,EntityNotFoundException  {
        Optional<User> user = getOptional(getUserRepository().findByUsername(username));

        if (user.isPresent()) {

            //verify the password
           if(bCryptPasswordEncoder.matches(password, user.get().getPassword())) {
               return mapDto(user.get());
           }else {
              throw new UnauthorizedException("the password is wrong!");
           }
        }

        // user does not exist
        throw new EntityNotFoundException(String.format("The user %s was not found!", username));
    }


    /**
     * toggle block status of a user
     * @param username the user name to be toggled blocked
     * @return the user after toggle blocking
     */
    public UserDto toggleBlock(String username){
        Optional<User> user = getOptional(getUserRepository().findByUsername(username));

        if (user.isEmpty()) {
            throw new EntityNotFoundException(String.format("The user %s was not found!", username));
        }

        user.get().setBlocked(!user.get().isBlocked());

        return saveUser( user.get(), false);
    }

    /**
     * @return the num if users that were registered this month
     */
    public int numOfUsersOfLastMonth(){
        Date firstOfMonth = Date.from(Timestamp.valueOf(LocalDate.now().atStartOfDay().withDayOfMonth(1)).toInstant());
        List<User> userList = userRepository.findByCreationAfter(firstOfMonth);

        return userList.size();
    }



    // ------------------------------------------------------- private methods -------------------------------------------------------

    /**
     * get an optional of a given user
     * @param user the user
     * @return an optional of the user
     */
    private Optional<User> getOptional(User user) {
        return Optional.ofNullable(user);
    }

    /**
     * map a user model to user dto
     * @param user the user to map
     * @return the user as dto
     */
    private UserDto mapDto(User user) {
        return userMapper.toUserDto(user);
    }

    /**
     * recives a password and encrypt it
     * @param password the password to encrypt
     * @return the encrypted password
     */
    private String encryptPassword(String password) {
        return getBCryptPasswordEncoder().encode(password);
    }

    /**
     * save a user to the DB and return the updated one
     * @throws NullPointerException if the user object is null
     * @param user the user to save
     * @param shouldEncrypt whether the password should be encrypted before saving
     * @return the new updated user's dto
     */
    private UserDto saveUser(User user, boolean shouldEncrypt) {

        if (user == null)
            throw new NullPointerException("Received a null user!");

        if(shouldEncrypt)
            user.setPassword(encryptPassword(user.getPassword())); // encrypt password

        return mapDto(getUserRepository().save(user));
    }

    /**
     *
     * @param email email
     * @return if email is valid
     */
    private boolean isValidEmailAddress(String email) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }
}
