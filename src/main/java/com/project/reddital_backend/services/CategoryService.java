package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.exceptions.DuplicateEntityException;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.repositories.CategoryRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Getter
@Setter
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * get a category dto buy its name
     * @param name the name of the category dto
     * @return the corresponding category dto
     */
    public CategoryDto findByName(String name) {
        return categoryMapper.toCategoryDto(categoryRepository.findByName(name));
    }

    /**
     * delete a category
     * @param toDelete category to delete
     */
    public void remove(String toDelete) {
        Optional<Category> cat = getOptional(getCategoryRepository().findByName(toDelete));
        cat.ifPresent(category -> getCategoryRepository().delete(category));
    }

    /**
     * @return all the categories
     */
    public List<CategoryDto> getAll() {
        return categoryRepository.findAll().stream()
                .map(categoryMapper::toCategoryDto)
                .collect(Collectors.toList());
    }

    /**
     * create a new category
     * @param toCreate the category to create
     * @return the created category
     */
    public CategoryDto create(String toCreate) {
        Optional<Category> cat = getOptional(getCategoryRepository().findByName(toCreate));

        if (cat.isEmpty()) {
            //No such cat exit, create the new sub object and save it.

            Category newSub = Category.builder()
                    .name(toCreate)
                    .build();

            return categoryMapper.toCategoryDto(categoryRepository.save(newSub)); // save new sub
        }

        // sub already exist. throw exception.
        throw new DuplicateEntityException("Failed to create new category, already exists : " + toCreate);
    }


    // --------------------------------------------------- private methods ---------------------------------------------------


    /**
     * get an optional of a given item
     * @param item the item
     * @return an optional of the item
     */
    private <T> Optional<T> getOptional(T item) {
        return Optional.ofNullable(item);
    }
}

