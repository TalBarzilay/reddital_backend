package com.project.reddital_backend.services;

import com.project.reddital_backend.DTOs.mappers.PostMapper;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.exceptions.*;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.SubReddit;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.PostRepository;
import com.project.reddital_backend.repositories.SubRedditRepository;
import com.project.reddital_backend.repositories.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Getter
@Setter
public class PostService {

    public enum Emotes {
        UpVote,
        DownVote
    }

    // ------------------------------------------------------- properties -------------------------------------------------------

    @Autowired
    private PostRepository postRepository; // access DB for post operations

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubRedditRepository subRedditRepository;

    @Autowired
    private PostMapper postMapper;


    // ------------------------------------------------------- methods -------------------------------------------------------

    public PostDto findById(long id){
        return postMapper.toPostDto(getPostRepository().findById(id).orElseThrow(() -> new EntityNotFoundException("No such post!")), null);
    }

    /**
     * post a post
     * @param postDto the post to be posted
     * @return the posted post object
     */
    public PostDto posting(PostDto postDto) {
        Optional<User> user = getOptional(userRepository.findByUsername(postDto.getUsername()));
        Optional<SubReddit> sub = getOptional(subRedditRepository.findByName(postDto.getSubReddit()));

        if(user.isEmpty()) {  // there is no such user, error!
            throw new UnauthorizedException("you do not have permission to post here!");
        }

        if(sub.isEmpty()) {  // there is no such user, error!
            throw new BadParametersException("no such sub exists : " + postDto.getSubReddit());
        }

        //saving the post
        Post post = Post.builder()
                .title(postDto.getTitle())
                .content(postDto.getContent())
                .subreddit(sub.get())
                .user(user.get())
                .build();

        // returned saved post
        return postMapper.toPostDto(postRepository.save(post), null);
    }


    /**
     * ge a list of the posts in a subreddit
     * @param sub the subreddit
     * @return a list of the post
     */
    public List<PostDto> postsOfSub(SubRedditDto sub, String curUser) {
        Optional<SubReddit> subReddit = getOptional(subRedditRepository.findByName(sub.getName()));

        if(subReddit.isEmpty()) {  // there is no such subreddit, error!
            throw new BadParametersException("no such sub exists : " + sub.getName());
        }

        List<Post> postsInSub = postRepository.findBySubredditOrderByLastUpdatedDesc(subReddit.get());

        // activate map on eac of the post to transform the lost to a lost of dto of posts.
        return postsInSub.stream()
                .map(post -> postMapper.toPostDto(post, curUser))
                .collect(Collectors.toList());
    }


    /**
     * add emotes for a post
     * if user downvoted, and now tries to upvote, he will be removed from the downvote list, and vice versa
     * @param username the name of the user that adds the emote
     * @param em the emote type
     * @return the postdto of the post after the emote
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    public  PostDto emote(long id, String username, Emotes em) throws EmoteAlreadyExistException {
        Optional<Post> post = getPostRepository().findById(id);
        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(username));

        if(post.isEmpty()) {
            throw new EntityNotFoundException("post was not found");
        }

        if(user.isEmpty()) {
            throw new EntityNotFoundException("user was not found : " + username);
        }

        if(post.get().isBlocked()){
            throw new PostBlockedException();
        }

        switch (em) {
            case UpVote: return upvote(post.get(), user.get());
            case DownVote: return downvote(post.get(), user.get());
        }

        throw new IllegalStateException("This line is here for compilation error");
    }


    /**
     * toggle blocking a post
     * @param id the id of the post
     * @return the new post after toggle blocking
     */
    public PostDto toggleBlock(long id){
        Optional<Post> post = getPostRepository().findById(id);

        if(post.isEmpty()) {
            throw new EntityNotFoundException("post was not found");
        }

        post.get().setBlocked(!post.get().isBlocked());

        return postMapper.toPostDto(postRepository.save(post.get()), null);
    }


    /**
     * eddit a post contnen
     * @param id the id of the post to edit
     * @param username the username of the edditor
     * @param content the new content of the post
     * @return the dto of the post after edit
     * @throws UnauthorizedException if trying to edit a post of different user
     */
    public  PostDto edit(long id, String username, String content, String title, boolean isAdmin) throws UnauthorizedException {
        Optional<Post> post = getPostRepository().findById(id);
        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(username));

        if(post.isEmpty()) {
            throw new EntityNotFoundException("post was not found");
        }

        if(user.isEmpty()) {
            throw new EntityNotFoundException("user was not found : " + username);
        }

        if(post.get().isBlocked()){
            throw new PostBlockedException();
        }

        if(!isAdmin && !post.get().getUser().equals(user.get())){
            throw new NotAllowedException("cannot edit post of different user!");
        }

        if(content != null)
            post.get().setContent(content);

        if(title != null)
            post.get().setTitle(title);

        return postMapper.toPostDto(postRepository.save(post.get()), user.get().getUsername());
    }


    /**
     * @return the num of posts that were registered this month
     */
    public int numOfPostsOfLastMonth(){
        Date firstOfMonth = Date.from(Timestamp.valueOf(LocalDate.now().atStartOfDay().withDayOfMonth(1)).toInstant());
        List<Post> postList = postRepository.findByCreationAfter(firstOfMonth);

        return postList.size();
    }


    // ------------------------------------------------------- private methods -------------------------------------------------------

    /**
     * get an optional of a given item
     * @param item the item
     * @return an optional of the item
     */
    private <T> Optional<T> getOptional(T item) {
        return Optional.ofNullable(item);
    }


    /**
     * downvote a post
     * if user upvoted, he will be removed from the upvote list
     * @param post the post
     * @param user the user that downvotes
     * @return the post dto after downvoting
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    private PostDto downvote(Post post, User user) throws EmoteAlreadyExistException  {
        if(post.getDownVotes().contains(user)) { // already downvoted, removes it
            post.getDownVotes().remove(user);
        } else {
            post.getUpVotes().remove(user);
            post.getDownVotes().add(user);
        }

        return postMapper.toPostDto(postRepository.save(post), user.getUsername());
    }

    /**
     * downvote a post
     * if user downvoted, and now tries to upvote, he will be removed from the downvote list
     * @param post the post
     * @param user the user that upvotes
     * @return the post dto after upvoting
     * @throws EmoteAlreadyExistException if trying to add an already exist emote
     */
    private PostDto upvote(Post post, User user) throws EmoteAlreadyExistException  {

        if(post.getUpVotes().contains(user)) {
            post.getUpVotes().remove(user); // already upvotes, remove it
        } else {
            post.getDownVotes().remove(user);
            post.getUpVotes().add(user);
        }

        return postMapper.toPostDto(postRepository.save(post), user.getUsername());
    }
}
