package com.project.reddital_backend.controllers.api;

import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.controllers.requests.AddSubRequest;
import com.project.reddital_backend.services.CategoryService;
import com.project.reddital_backend.services.SubRedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/subredditals")
public class SubRedditalController {

    @Autowired
    private SubRedditService subRedditService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubRedditMapper subRedditMapper;


    @CrossOrigin
    @PostMapping("/create")
    public ResponseEntity<SubRedditDto> createSub(@RequestBody AddSubRequest request) {
        request.validate();

        return ResponseEntity.created(URI.create("subredditals/create"))
                .body(create(request));
    }

    @CrossOrigin
    @PostMapping("/delete")
    public ResponseEntity<Void> removeCat(@PathParam("subreddital") String subreddital) {
        remove(subreddital);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    // ------------------------------------------- private methods -------------------------------------------

    private SubRedditDto create(AddSubRequest request) {
        return subRedditService.create(subRedditMapper.toSubRedditDto(request));
    }

    private void remove(String sub) {
        subRedditService.remove(sub);
    }


    /**
     * @return a map when the key is a category, the value is the subs in this category
     */
    private Map<CategoryDto, List<SubRedditDto>> getByCategory() {
        Map<CategoryDto, List<SubRedditDto>> result = new HashMap<>();

        // get the list of all categories
        List<CategoryDto> categories = categoryService.getAll();

        // for eac category, get the list of all subs
        categories.forEach(categoryDto -> {
            List<SubRedditDto> subsInCat = subRedditService.findByCategory(categoryDto);

            result.put(categoryDto, subsInCat);
        });

        //return the map
        return result;
    }
}
