package com.project.reddital_backend.controllers.api;

import com.project.reddital_backend.DTOs.mappers.CommentMapper;
import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.controllers.requests.EditRequest;
import com.project.reddital_backend.controllers.requests.EmoteRequest;
import com.project.reddital_backend.controllers.requests.PostCommentRequest;
import com.project.reddital_backend.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping("/comments")
public class CommentController extends BaseController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentMapper mapper;



    @CrossOrigin
    @PostMapping("/commenting")
    public ResponseEntity<CommentDto> createSub(@RequestBody PostCommentRequest request, @AuthenticationPrincipal Object principal) {
        request.validate();

        return ResponseEntity.created(URI.create("/comments/commenting"))
                .body(comment(request, principl2string(principal)));
    }


    @CrossOrigin
    @PostMapping("/emote")
    public ResponseEntity<CommentDto> emote(@RequestBody EmoteRequest request, @AuthenticationPrincipal Object principal) {
        request.validate();

        return ResponseEntity.ok()
                .body(emoting(request.getId(), principl2string(principal), request.getEmote()));
    }


    @CrossOrigin
    @PostMapping("/editComment")
    public ResponseEntity<CommentDto> edit(@RequestBody EditRequest request, @AuthenticationPrincipal Object principal, Authentication authentication) {
        request.validate();

        return ResponseEntity.ok()
                .body(editing(request.getId(), principl2string(principal), request.getContent(), authentication.getAuthorities()));
    }


    // ------------------------------------------------ private methods ------------------------------------------------


    private CommentDto comment(PostCommentRequest request, String username) {
        return commentService.postComment(mapper.toCommentDto(request, username));
    }


    private CommentDto emoting(long id, String username, String em) {

        CommentService.Emotes e =
                em.equals("upvote") ? CommentService.Emotes.UpVote :
                        em.equals("downvote") ? CommentService.Emotes.DownVote :
                                null;

        return commentService.emote(id, username, e);
    }

    private CommentDto editing(long id, String username, String content, Collection<? extends GrantedAuthority> authorities) {
        return commentService.edit(id, username, content, authorities.stream().parallel().anyMatch(auth -> auth.getAuthority().equals("ADMIN")));
    }
}
