package com.project.reddital_backend.controllers.api;

import com.project.reddital_backend.DTOs.mappers.PostMapper;
import com.project.reddital_backend.DTOs.mappers.SubRedditMapper;
import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.controllers.requests.EditRequest;
import com.project.reddital_backend.controllers.requests.EmoteRequest;
import com.project.reddital_backend.controllers.requests.PostingRequest;
import com.project.reddital_backend.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.Collection;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PostController extends BaseController {

    @Autowired
    private PostService postService;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private SubRedditMapper subMapper;


    @CrossOrigin
    @PostMapping("/posting")
    public ResponseEntity<PostDto> postingPost(@RequestBody @Valid PostingRequest postingRequest, @PathParam("subreddital") String subreddital, @AuthenticationPrincipal Object principal) {
        postingRequest.validate();

        return ResponseEntity.created(URI.create("/posting/" + subreddital.replace(" ", "%20")))
                .body(posting(postingRequest, subreddital, principl2string(principal)));
    }

    @CrossOrigin
    @GetMapping("/posts")
    public ResponseEntity<List<PostDto>> postsOfSub(@PathParam("subreddital") String subreddital, @AuthenticationPrincipal Object principal) {
        return ResponseEntity.ok()
                .body(postsInsASub(subreddital, principl2string(principal)));
    }

    @CrossOrigin
    @GetMapping("/post")
    public ResponseEntity<PostDto> getAPost(@PathParam("id") long id) {
        return ResponseEntity.ok()
                .body(getPost(id));
    }

    @CrossOrigin
    @PostMapping("/emote")
    public ResponseEntity<PostDto> emote(@RequestBody EmoteRequest request, @AuthenticationPrincipal Object principal) {
        request.validate();

        return ResponseEntity.ok()
                .body(emoting(request.getId(), principl2string(principal), request.getEmote()));
    }


    @CrossOrigin
    @PostMapping("/blockPost")
    public ResponseEntity<PostDto> block(@PathParam("id") long id) {
        return ResponseEntity.ok()
                .body(blocking(id));
    }

    @CrossOrigin
    @PostMapping("/editPost")
    public ResponseEntity<PostDto> edit(@RequestBody EditRequest request, @AuthenticationPrincipal Object principal, Authentication authentication) {
        request.validate();

        return ResponseEntity.ok()
                .body(editing(request.getId(), principl2string(principal), request.getContent(), request.getTitle(), authentication.getAuthorities()));
    }


    // ----------------------------------------- private methods ----------------------------------------------

    /**
     * post a post
     * @param toPost the posting request
     * @param subreddital the subreddit to post to
     * @return the posted post's dto
     */
    private PostDto posting(PostingRequest toPost, String subreddital, String username) {
        return postService.posting(postMapper.toPostDto(toPost, subreddital, username));
    }

    private List<PostDto> postsInsASub(String subreddital, String curUser) {
        return postService.postsOfSub(subMapper.toSubRedditDto(subreddital), curUser);
    }

    private PostDto emoting(long id, String username, String em) {

        PostService.Emotes e =
                em.equals("upvote") ? PostService.Emotes.UpVote :
                em.equals("downvote") ? PostService.Emotes.DownVote :
                        null;

        return postService.emote(id, username, e);
    }

    private PostDto editing(long id, String username, String content, String title, Collection<? extends GrantedAuthority> authorities) {
        return postService.edit(id, username, content, title, authorities.stream().parallel().anyMatch(auth -> auth.getAuthority().equals("ADMIN")));
    }


    private PostDto getPost(long id) {
        return postService.findById(id);
    }

    private PostDto blocking(long id){
        return postService.toggleBlock(id);
    }

}
