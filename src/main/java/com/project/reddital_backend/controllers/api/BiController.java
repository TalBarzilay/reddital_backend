package com.project.reddital_backend.controllers.api;

import com.project.reddital_backend.DTOs.models.BiDto;
import com.project.reddital_backend.services.BiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("")
public class BiController {

    @Autowired
    private BiService biService;

    @CrossOrigin
    @GetMapping("/bi")
    public ResponseEntity<BiDto> bi() {
        return ResponseEntity.ok(biService.bi());
    }
}
