package com.project.reddital_backend.controllers.api;

import com.project.reddital_backend.DTOs.mappers.CategoryMapper;
import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryMapper catMapper;


    @CrossOrigin
    @PostMapping("/create")
    public ResponseEntity<CategoryDto> createCat(@PathParam("category") String category) {
        return ResponseEntity.created(URI.create("/categories/create"))
                .body(create(category));
    }

    @CrossOrigin
    @PostMapping("/delete")
    public ResponseEntity<Void> removeCat(@PathParam("category") String category) {
        remove(category);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @CrossOrigin
    @GetMapping(value = {"/all", "/editable"})
    public ResponseEntity<List<CategoryDto>> getAll() {
        return ResponseEntity.ok()
                .body(findAll());
    }


    // ------------------------------------------- private methods -------------------------------------------

    private CategoryDto create(String category) {
        return categoryService.create(category);
    }

    private void remove(String cat) {
        categoryService.remove(cat);
    }

    private List<CategoryDto> findAll() {
        return categoryService.getAll();
    }
}
