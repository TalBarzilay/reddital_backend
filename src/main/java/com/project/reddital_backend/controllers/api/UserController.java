package com.project.reddital_backend.controllers.api;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.project.reddital_backend.DTOs.mappers.UserMapper;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.controllers.requests.ChangePasswordRequest;
import com.project.reddital_backend.controllers.requests.LoginRequest;
import com.project.reddital_backend.controllers.requests.SignupRequest;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.exceptions.RefreshTokenExpiredException;
import com.project.reddital_backend.exceptions.UnauthorizedException;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.CustomUserDetailsService;
import com.project.reddital_backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.stream.Collectors;


@RestController
@CrossOrigin
@RequestMapping("/")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private AlgorithmProvider algorithmProvider;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;




    @CrossOrigin
    @PostMapping("/signup")
    public ResponseEntity<UserDto> signup(@RequestBody @Valid SignupRequest signupRequest) {
        signupRequest.validate();

        return ResponseEntity.created(URI.create("/signup"))
                .body(registerUser(signupRequest));
    }


    @CrossOrigin
    @PostMapping("/changePassword")
    public ResponseEntity<UserDto> changePass(@RequestBody ChangePasswordRequest request, @AuthenticationPrincipal Object principal) {
        return ResponseEntity.ok()
                .body(changePassword(principl2string(principal), request.getPassword(), request.getOldPassword()));
    }

    @CrossOrigin
    @PostMapping("/updateProfile")
    public ResponseEntity<UserDto> updateProfile(@PathParam("email") String email,  @AuthenticationPrincipal Object principal) {
        return ResponseEntity.ok()
                .body(updatingProfile(UserDto.builder().username(principl2string(principal)).email(email).build()));
    }


    @CrossOrigin
    @PostMapping("/blockUser")
    public ResponseEntity<UserDto> signup(@PathParam("username") String username) {
        return ResponseEntity.ok()
                .body(blocking(username));
    }


    @CrossOrigin
    @PostMapping("/refreshAccessToken")
    public ResponseEntity<String> refreshToken(HttpServletRequest request, HttpServletResponse response) {
        String refresh_token = request.getHeader(HttpHeaders.AUTHORIZATION);

        try {
            JWTVerifier verifier = JWT.require(algorithmProvider.getAlgorithm()).build();
            DecodedJWT decoded = verifier.verify(refresh_token);

            UserDetails user = customUserDetailsService.loadUserByUsername(decoded.getSubject());

            if(user == null) {
                throw new EntityNotFoundException("your user could non be verified");
            }

            String new_access = tokenProvider.generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()),  request.getRequestURL().toString());

            return ResponseEntity.ok()
                    .body(new_access);

        } catch (TokenExpiredException ex) {
            throw new RefreshTokenExpiredException("the refresh token is expired!");
        }
        catch (Exception e){
            throw new UnauthorizedException("failed to verify your refresh token");
        }
    }




    // -------------------------------------- private methods --------------------------------------

    /**
     * receive a request ro user registration, and register it
     * @param signupRequest the request
     * @return the user that was saved to the DB
     */
    private UserDto registerUser(SignupRequest signupRequest) {
        return userService.signup(userMapper.toUserDto(signupRequest));
    }

    private UserDto blocking(String username){
        return  userService.toggleBlock(username);
    }

    private UserDto changePassword(String username, String password, String oldPass){
        return userService.changePassword(username, password, oldPass);
    }

    /**
     * perform a login
     * @param loginRequest the login request object
     * @return a user DTO of the user that was logged in
     */
    private UserDto loginUser(LoginRequest loginRequest) {
        return userService.login(loginRequest.getUsername(), loginRequest.getPassword());
    }

    private UserDto updatingProfile(UserDto toUpdate){
        return userService.updateProfile(toUpdate);
    }


}
