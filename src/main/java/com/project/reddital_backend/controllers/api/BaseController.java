package com.project.reddital_backend.controllers.api;

public class BaseController {

    protected String principl2string(Object principle){
        if(principle == null){
            return  null;
        }

        if("anonymousUser".equals(principle)){
            return null;
        }

        return (String) principle;
    }
}
