package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PostingRequest extends Request {

    private String title;

    private String content;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

        if(nullOrEmpty(title)) {
            ans = "title is not valid";
        }

        if(ans != null){
            throw new BadParametersException(ans);
        }
    }
}
