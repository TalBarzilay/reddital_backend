package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EditRequest extends Request{

    private long    id;
    private String content;
    private String title;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

        if(id < 0 ){
            ans = "id can't be negative!";
        } else if(content == null) {
            ans = "invalid content!";
        } else if(title != null && title.equals("")) {
            ans = "title content!";
        }

        if(ans != null)
            throw new BadParametersException(ans);
    }
}