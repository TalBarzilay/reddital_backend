package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PostCommentRequest extends Request{


    private String content;

    private Long responseTo;

    private Long postedOn;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

        if(nullOrEmpty(content)){
            ans = "no content is specified!";
        } else if(responseTo == null && postedOn == null){
            ans = "no father is specified";
        } else if(responseTo != null && postedOn != null){
            ans = "comment must belong ether to a post or comment, but not both!";
        }

        if(ans != null){
            throw new BadParametersException(ans);
        }
    }
}
