package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ChangePasswordRequest extends Request{

    private String password;
    private String oldPassword;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

       if(nullOrEmpty(password)) {
            ans = "bad new password";
        }  else if(nullOrEmpty(oldPassword)) {
            ans = "bad new password";
        }

        if(ans != null)
            throw new BadParametersException(ans);
    }
}