package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddSubRequest  extends Request{

    private String name;

    private String category;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

        if(nullOrEmpty(name)) {
            ans = "name of subreddital is not valid";
        } else if(nullOrEmpty(category)) {
            ans = "name of category is not valid";
        }

        if(ans != null){
            throw new BadParametersException(ans);
        }
    }
}
