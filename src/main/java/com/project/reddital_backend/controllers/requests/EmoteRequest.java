package com.project.reddital_backend.controllers.requests;

import com.project.reddital_backend.exceptions.BadParametersException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EmoteRequest extends Request{

    private long    id;
    private String  emote;

    @Override
    public void validate() throws BadParametersException {
        String ans = null;

        List<String> allowedEmotes = List.of("upvote", "downvote");

        if(id < 0 ){
            ans = "post id can't be negative!";
        } else if(nullOrEmpty(emote)) {
            ans = "invalid emote!";
        } else if(!allowedEmotes.contains(emote)) {
            ans = "emote is not recognized!";
        }

        if(ans != null)
            throw new BadParametersException(ans);
    }
}
