package com.project.reddital_backend.DTOs.models;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@Builder
@Accessors(chain = true)
public class BiDto {

    private int newUsers;

    private int newPosts;

    private Map<String, Integer> postsOfSub;
}
