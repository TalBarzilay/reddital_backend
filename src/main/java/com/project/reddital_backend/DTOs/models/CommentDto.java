package com.project.reddital_backend.DTOs.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Objects;

@Data
@Builder
@Accessors(chain = true)
public class CommentDto {

    private long id;

    private String content;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private CommentDto responseTo;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private PostDto postedOn;

    private String user;

    @EqualsAndHashCode.Exclude
    private Long creation;

    @EqualsAndHashCode.Exclude
    private Long updated;

    private double upVotes;

    private double downVotes;

    @Builder.Default
    private List<String> emotes = List.of();

    private List<CommentDto> comments;

}
