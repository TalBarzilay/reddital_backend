package com.project.reddital_backend.DTOs.models;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class CategoryDto {
    private long id;

    private String name;

    private List<SubRedditDto> subredditals;
}