package com.project.reddital_backend.DTOs.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class PostDto {

    private long id;

    private String title;

    private String content;

    @EqualsAndHashCode.Exclude
    private Long creation;

    @EqualsAndHashCode.Exclude
    private Long lastUpdated;

    private String username;

    private boolean isBlocked;

    private String subReddit;

    private double upVotes;

    private double downVotes;

    @Builder.Default
    private List<String> emotes = List.of();

    private List<CommentDto> comments;
}
