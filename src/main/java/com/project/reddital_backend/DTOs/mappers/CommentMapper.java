package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.CommentDto;
import com.project.reddital_backend.controllers.requests.PostCommentRequest;
import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.services.CommentService;
import com.project.reddital_backend.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommentMapper {

    @Autowired
    private PostService postService;

    @Autowired
    private CommentService commentService;

    public CommentDto toCommentDto(Comment comment, String logedUser){
        if(comment == null)
            return null;

        return CommentDto.builder()
                .id(comment.getId())
                .content(comment.getContent())
                .user(comment.getUser() == null? null : comment.getUser().getUsername())
                .creation(comment.getCreation() == null? null : comment.getCreation().getTime())
                .updated(comment.getUpdated() == null? null : comment.getUpdated().getTime())
                .upVotes(comment.getUpVotes() == null? 0 : comment.getUpVotes().size())
                .downVotes(comment.getDownVotes() == null? 0 : comment.getDownVotes().size())
                .emotes(getEmotes(comment, logedUser))
                .comments(comment.getComments() == null? null : comment.getComments().stream()
                        .map(commentObj -> toCommentDto(commentObj, logedUser)).collect(Collectors.toList()))
                .build();
    }

    public CommentDto toCommentDto(PostCommentRequest request, String username){
        if(request == null || username == null)
            return null;

        return CommentDto.builder()
                .content(request.getContent())
                .user(username)
                .postedOn(request.getPostedOn() == null ? null : postService.findById(request.getPostedOn()))
                .responseTo(request.getResponseTo() == null? null : commentService.findById(request.getResponseTo()))
                .comments(null)
                .build();
    }






    private List<String> getEmotes(Comment comment, String logedUser) {
        List<String>  emotes = new ArrayList<>();

        if(logedUser == null || logedUser.equals("")){
            return  emotes;
        }

        if(comment.getUpVotes().stream().map(User::getUsername).collect(Collectors.toList()).contains(logedUser)) {
            emotes.add("upvote");
        }

        if(comment.getDownVotes().stream().map(User::getUsername).collect(Collectors.toList()).contains(logedUser)) {
            emotes.add("downvote");
        }

        return emotes;
    }
}
