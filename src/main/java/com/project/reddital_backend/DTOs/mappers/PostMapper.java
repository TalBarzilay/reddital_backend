package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.PostDto;
import com.project.reddital_backend.controllers.requests.PostingRequest;
import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostMapper {

    @Autowired
    private CommentMapper commentMapper;

    /**
     * transform a post object into a post dto
     * @param post the post object
     * @return the mapping of the post object to post dto
     */
    public PostDto toPostDto(Post post, String curUser) {

        if(post == null)
            return null;

        return PostDto.builder()
                .id(post.getId())
                .title(post.getTitle())
                .content(post.getContent())
                .isBlocked(post.isBlocked())
                .creation(post.getCreation() == null? null : post.getCreation().getTime())
                .lastUpdated(post.getLastUpdated() == null? null : post.getLastUpdated().getTime())
                .subReddit(post.getSubreddit().getName())
                .username(post.getUser().getUsername())
                .upVotes(post.getUpVotes() == null ? 0 : post.getUpVotes().size())
                .downVotes(post.getDownVotes() == null ? 0 : post.getDownVotes().size())
                .emotes(getEmotes(post, curUser))
                .comments(post.getComments() == null ? null : post.getComments().stream()
                        .map(comment -> commentMapper.toCommentDto(comment, curUser)).collect(Collectors.toList()))
                .build();
    }
    /**
     * transform a posting request object into a post dto. transform authentication key into a user.
     * @param postingRequest the posting request
     * @param subreddital the subreddit the request is for
     * @return the mapping of the posting request to post dto
     */
    public PostDto toPostDto(PostingRequest postingRequest, String subreddital, String username) {

        if(postingRequest == null || subreddital == null)
            return null;

        return PostDto.builder()
                .title(postingRequest.getTitle())
                .content(postingRequest.getContent())
                .subReddit(subreddital)
                .username(username)
                .build();
    }








    private List<String> getEmotes(Post post, String curUser) {
        List<String>  emotes = new ArrayList<>();

        if(curUser == null || curUser.equals("")){
            return  emotes;
        }

        if(post.getUpVotes().stream().map(User::getUsername).collect(Collectors.toList()).contains(curUser)) {
            emotes.add("upvote");
        }

        if(post.getDownVotes().stream().map(User::getUsername).collect(Collectors.toList()).contains(curUser)) {
            emotes.add("downvote");
        }

        return emotes;
    }

}
