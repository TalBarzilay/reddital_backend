package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.RoleDto;
import com.project.reddital_backend.models.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {

    /**
     * transform a role object to role dto
     * @param role the role
     * @return the mapping of the role into a dto
     */
    public RoleDto toRoleDto(Role role) {

        if(role == null)
            return null;

        return RoleDto.builder()
                .role(role.getRole())
                .build();
    }

    public GrantedAuthority toAuthority(Role role){
        return  new SimpleGrantedAuthority(role.getRole());
    }
}
