package com.project.reddital_backend.DTOs.mappers;

import com.project.reddital_backend.DTOs.models.CategoryDto;
import com.project.reddital_backend.DTOs.models.SubRedditDto;
import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.models.SubReddit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryMapper {

    @Autowired
    private SubRedditMapper subMapper;

    /**
     * transform a category object to category dto
     * @param cat the category
     * @return the mapping of the category into a dto
     */
    public CategoryDto toCategoryDto(Category cat) {

        if(cat == null)
            return null;

        return CategoryDto.builder()
                .id(cat.getId())
                .name(cat.getName())
                .subredditals(mapSubs(cat.getSubRedditals()))
                .build();
    }

    public CategoryDto toCategoryDto(String category) {
        if(category == null)
            return null;

        return CategoryDto.builder()
                .name(category)
                .build();
    }




    private List<SubRedditDto> mapSubs(List<SubReddit> subRedditals) {
        if(subRedditals == null){
            return null;
        }

        return subRedditals.stream()
                .map(subMapper::toSubRedditDto)
                .collect(Collectors.toList());
    }

}
