package com.project.reddital_backend;

import com.project.reddital_backend.models.Role;
import com.project.reddital_backend.models.User;
import com.project.reddital_backend.repositories.RoleRepository;
import com.project.reddital_backend.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@SpringBootApplication
public class RedditalBackendApplication {

	// main method
	public static void main(String[] args) {
		SpringApplication.run(RedditalBackendApplication.class, args);
	}

	// initiate roles, and Admin
	@Bean
	public CommandLineRunner loadData(RoleRepository roleRepository, UserRepository userRepository, BCryptPasswordEncoder encoder) {
		return args -> {

			try {

				if(userRepository.findByUsername("Admin") != null){
					return; // admin already exist
				}

				// save USER, ADMIN roles
				Role userRole 	= roleRepository.findByRole("USER");
				Role adminRole 	= roleRepository.findByRole("ADMIN");


				if(userRole == null)
					userRole 	= roleRepository.save(Role.builder().role("USER").build());

				if(adminRole == null)
					adminRole 	= roleRepository.save(Role.builder().role("ADMIN").build());

				User admin = User.builder()
						.username("Admin")
						.password(encoder.encode("Admin123456"))
						.email("admin@reddital.com")
						.roles(List.of(userRole, adminRole))
						.build();

				userRepository.save(admin); //create default Admin
			} catch (Exception ignored){}
		};
	}

}
