package com.project.reddital_backend.config;

import com.project.reddital_backend.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@CrossOrigin
public class ExceptionController {

    //The goal of this controller is to handle exceptions

    // -------------------------------------- exception handlers --------------------------------------

    @ExceptionHandler(DuplicateEntityException.class)
    public final ResponseEntity<String> handle(DuplicateEntityException ex) {
        return ResponseEntity.unprocessableEntity()
                .body(ex.getMessage());
    }

    @ExceptionHandler(BadParametersException.class)
    public final ResponseEntity<String> handle(BadParametersException ex) {
        return ResponseEntity.badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public final ResponseEntity<String> handle(EntityNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(ex.getMessage());
    }

    @ExceptionHandler(UnauthorizedException.class)
    public final ResponseEntity<String> handle(UnauthorizedException ex) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ex.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public final ResponseEntity<String> handle(UsernameNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(ex.getMessage());
    }

    @ExceptionHandler(AccessTokenExpiredException.class)
    public final ResponseEntity<String> handle(AccessTokenExpiredException ex) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ex.getMessage());
    }

    @ExceptionHandler(RefreshTokenExpiredException.class)
    public final ResponseEntity<String> handle(RefreshTokenExpiredException ex) {
        return ResponseEntity
                .status(402)
                .body(ex.getMessage());
    }


    @ExceptionHandler(EmoteAlreadyExistException.class)
    public final ResponseEntity<String> handle(EmoteAlreadyExistException ex) {
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(ex.getMessage());
    }


    @ExceptionHandler(UserBlockedException.class)
    public final ResponseEntity<String> handle(UserBlockedException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_ACCEPTABLE)
                .body(ex.getMessage());
    }

    @ExceptionHandler(PostBlockedException.class)
    public final ResponseEntity<String> handle(PostBlockedException ex) {
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(ex.getMessage());
    }

    @ExceptionHandler(NotAllowedException.class)
    public final ResponseEntity<String> handle(NotAllowedException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }






    @ExceptionHandler(Exception.class)
    public final ResponseEntity<String> handle(Exception ex) {
        ex.printStackTrace();

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("your request was denied by the server.");
    }

}
