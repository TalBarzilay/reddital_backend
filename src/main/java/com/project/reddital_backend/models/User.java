package com.project.reddital_backend.models;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private long id; //PK


    @Column(name = "username", unique = true)
    private String username; //Username (UNIQUE)


    @Column(name = "email")
    private String email; // user's email


    @Column(name = "password")
    private String password; // user's hashed password

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation", updatable = false)
    private Date creation;

    @Column
    @Builder.Default
    private boolean isBlocked = false;


    @ManyToMany(fetch = FetchType.EAGER)
    private List<Role> roles;
}