package com.project.reddital_backend.models;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "posts")
public class Post {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id; //PK


    @Column(name = "title" , length = 512)
    private String title;

    @Column(name = "content", columnDefinition="TEXT")
    private String content;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation", updatable = false)
    private Date creation;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastUpdated")
    private Date lastUpdated;

    @ManyToMany
    @Builder.Default
    private List<User> upVotes = Collections.synchronizedList(new ArrayList<>());

    @ManyToMany
    @Builder.Default
    private List<User> downVotes = Collections.synchronizedList(new ArrayList<>());

    @Column
    @Builder.Default
    private boolean isBlocked = false;

    @ManyToOne
    private User user;

    @ManyToOne
    private SubReddit subreddit;



    @OneToMany(mappedBy = "postedOn", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Comment> comments;

}
