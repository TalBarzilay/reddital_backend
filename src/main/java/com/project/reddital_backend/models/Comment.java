package com.project.reddital_backend.models;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id; //

    @Column(name = "content")
    private String content;


    //@Column(name = "responseTo")
    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Comment responseTo;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Post postedOn;

    @ManyToOne
    private User user;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation", updatable = false)
    private Date creation;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated")
    private Date updated;


    @ManyToMany
    @Builder.Default
    private List<User> upVotes = Collections.synchronizedList(new ArrayList<>());

    @ManyToMany
    @Builder.Default
    private List<User> downVotes = Collections.synchronizedList(new ArrayList<>());



    @OneToMany(mappedBy = "responseTo", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Comment> comments;
}
