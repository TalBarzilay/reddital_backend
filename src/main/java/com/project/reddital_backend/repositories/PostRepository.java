package com.project.reddital_backend.repositories;

import com.project.reddital_backend.models.Post;
import com.project.reddital_backend.models.SubReddit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository  extends JpaRepository<Post, Long> {

    /**
     * find all post from a certain subreddit, and order tam by the date.
     * @param subReddit the sub reddit
     * @return all the post from that subreddit
     */
    List<Post> findBySubredditOrderByLastUpdatedDesc(SubReddit subReddit);

    /**
     * @param date the date to return posts created after
     * @return list of posts that were created after a certain date
     */
    List<Post> findByCreationAfter(Date date);
}
