package com.project.reddital_backend.repositories;

import com.project.reddital_backend.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CategoryRepository  extends JpaRepository<Category, Long> {

    /**
     * return a category by its unique name
     * @param name the name to find the category by
     * @return the category by the given name
     */
    Category findByName(String name);

    /**
     * @return all categories
     */
    List<Category> findAll();
}