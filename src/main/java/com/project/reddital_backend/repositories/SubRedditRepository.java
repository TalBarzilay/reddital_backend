package com.project.reddital_backend.repositories;

import com.project.reddital_backend.models.Category;
import com.project.reddital_backend.models.SubReddit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SubRedditRepository  extends JpaRepository<SubReddit, Long> {

    /**
     * return a sub by its unique name
     * @param name the name to find the sub by
     * @return the sub by the given name
     */
    SubReddit findByName(String name);

    /**
     * ge a list of all subs in a category
     * @param cat the category
     * @return  list of all subs in the category
     */
    List<SubReddit> findByCategory(Category cat);
}
