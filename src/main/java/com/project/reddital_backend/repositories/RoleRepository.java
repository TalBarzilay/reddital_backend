package com.project.reddital_backend.repositories;

import com.project.reddital_backend.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    /**
     * ge a role by iy name
     * @param role the role's name
     * @return the role object
     */
    Role findByRole(String role);
}
