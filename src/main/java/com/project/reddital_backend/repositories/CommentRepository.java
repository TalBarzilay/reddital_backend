package com.project.reddital_backend.repositories;


import com.project.reddital_backend.models.Comment;
import com.project.reddital_backend.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository  extends JpaRepository<Comment, Long> {

    /**
     * gett all comment on a post
     * @param post the post
     * @return a list of the comments (including nested comments) that was made on this post
     */
    List<Comment> findByPostedOn(Post post);
}
