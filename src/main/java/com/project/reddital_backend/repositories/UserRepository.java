package com.project.reddital_backend.repositories;

import com.project.reddital_backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * return a user by its unique primary key
     * @param id the PK to find the user by
     * @return the user by the given ID
     */
    User findById(long id);

    /**
     * return a user by its unique username
     * @param username the username to find the user by
     * @return the user by the given username
     */
    User findByUsername(String username);

    /**
     * @param date the date to return user created after
     * @return list of users that were created after a certain date
     */
    List<User> findByCreationAfter(Date date);
}
