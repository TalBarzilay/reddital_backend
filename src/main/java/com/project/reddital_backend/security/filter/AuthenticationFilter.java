package com.project.reddital_backend.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.reddital_backend.DTOs.models.UserDto;
import com.project.reddital_backend.controllers.requests.LoginRequest;
import com.project.reddital_backend.exceptions.BadParametersException;
import com.project.reddital_backend.exceptions.EntityNotFoundException;
import com.project.reddital_backend.exceptions.UnauthorizedException;
import com.project.reddital_backend.exceptions.UserBlockedException;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import com.project.reddital_backend.security.providers.TokenProvider;
import com.project.reddital_backend.services.UserService;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AlgorithmProvider algorithmProvider;

    private final ObjectMapper objectMapper;

    private final TokenProvider tokenProvider;

    private final UserService userService;




    private static class AuthException extends AuthenticationException {
        public AuthException(String msg, Throwable cause) {
            super(msg, cause);
        }
    }




    public AuthenticationFilter(AuthenticationManager authenticationManager, UserService userService, AlgorithmProvider algorithmProvider, TokenProvider tokenProvider, ObjectMapper objectMapper) {
        super(authenticationManager);
        this.algorithmProvider = algorithmProvider;
        this.objectMapper = objectMapper;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {

            LoginRequest req = objectMapper.readValue(request.getInputStream(),LoginRequest.class);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword());

            return getAuthenticationManager().authenticate(authenticationToken);


        } catch (EntityNotFoundException ex) {
            throw new AuthException("" + HttpServletResponse.SC_NOT_FOUND, ex);
        } catch (UnauthorizedException | BadCredentialsException ex) {
            throw new AuthException("" + HttpServletResponse.SC_UNAUTHORIZED, ex);
        } catch (BadParametersException ex) {
            throw new AuthException("" + HttpServletResponse.SC_BAD_REQUEST, ex);
        } catch (Exception e){
            throw new AuthException("" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR, new RuntimeException("error accrued."));
        }
    }

    @Override
    public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws ServletException, IOException {
        // we need to return the user their access & refresh token

        User user = (User) authentication.getPrincipal();
        UserDto udto = userService.findUserByUsername(user.getUsername());

        if(udto.isBlocked()){
            throw new AuthException("" + HttpServletResponse.SC_BAD_REQUEST, new UserBlockedException("You have been blocked!"));
        }

        // set access & refresh tokens in the header
        response.setHeader("access_token", tokenProvider.generateAccessToken(user, request.getRequestURL().toString()));
        response.setHeader("refresh_token", tokenProvider.generateRefreshToken(user, request.getRequestURL().toString()));

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(response.getOutputStream(), udto);
    }


    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        int status = Integer.parseInt(failed.getMessage());
        String msg = failed.getCause().getMessage();


        response.resetBuffer();
        response.setStatus(status);
        response.getOutputStream().print(msg);
        response.flushBuffer();
    }
}
