package com.project.reddital_backend.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.project.reddital_backend.security.providers.AlgorithmProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {

   private final AlgorithmProvider algorithmProvider;

    public AuthorizationFilter(AlgorithmProvider algorithmProvider) {
        this.algorithmProvider = algorithmProvider;
    }

    // perform authorization of the user, checking the access token.
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if(request.getRequestURI().equals("/login") || request.getRequestURI().equals("/refreshAccessToken")){
            filterChain.doFilter(request, response); // Do nothing, user is trying to login
            return;
        }

        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if(token == null){
            filterChain.doFilter(request, response); // a guest, perhaps the request can still be processed.
            return;
        }

        try {
            JWTVerifier verifier = JWT.require(algorithmProvider.getAlgorithm()).build();
            DecodedJWT decoded = verifier.verify(token);

            String          username    = decoded.getSubject();
            List<String>    roles       = decoded.getClaim("roles").asList(String.class);
            Collection<SimpleGrantedAuthority> authorities = roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, null, authorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            filterChain.doFilter(request, response); // continue processing the request

        } catch (Exception ex) {
            response.sendError(HttpStatus.FORBIDDEN.value(), ex.getMessage());
        }

    }
}
