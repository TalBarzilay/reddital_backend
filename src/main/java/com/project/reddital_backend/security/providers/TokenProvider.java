package com.project.reddital_backend.security.providers;

import com.auth0.jwt.JWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TokenProvider {

    @Autowired
    private AlgorithmProvider algorithmProvider;

    public String generateAccessToken(org.springframework.security.core.userdetails.User user, String issuer) {
        return generateAccessToken(user.getUsername(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), issuer);
    }

    public String generateAccessToken(String username, List<String> roles, String issuer) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(tokenExpiration(10))
                .withIssuer(issuer)
                .withClaim("roles", roles)
                .sign(algorithmProvider.getAlgorithm());
    }

    public String generateRefreshToken(org.springframework.security.core.userdetails.User user, String issuer) {
        return generateRefreshToken(user.getUsername(), issuer);
    }

    public String generateRefreshToken(String username, String issuer) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(tokenExpiration(60 * 24))
                .withIssuer(issuer)
                .sign(algorithmProvider.getAlgorithm());
    }



    // -------------------------------------------------- private methods --------------------------------------------------

    /**
     * retirn the expiration date for a token
     * @param validTimeInMinutes the amount on minutes the token is valid
     * @return a date of the expiration date (now() + accessValidTimeInMinutes)
     */
    private Date tokenExpiration(double validTimeInMinutes){
        double validTimeInSecond = validTimeInMinutes * 60;
        int validTimeInMiliSecond = (int) (validTimeInSecond * 1000);

        return new Date(new Date().getTime() + validTimeInMiliSecond);
    }
}
