package com.project.reddital_backend.security.providers;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AlgorithmProvider {

    @Autowired
    private SecretProvider secretProvider;


    public Algorithm getAlgorithm() {
        return Algorithm.HMAC256(secretProvider.getSecret().getBytes());
    }


}
