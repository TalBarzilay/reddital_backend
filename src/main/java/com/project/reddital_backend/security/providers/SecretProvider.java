package com.project.reddital_backend.security.providers;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class SecretProvider {

    /**
     * this is our secret. this is a TOP SECRET. this cannot be known to ANYONE.
     * need to be as random as posible.
     * smashing the keyboard is NOT random enough! use random string generators to give this secret a value!
     */
    private final String secret = "IOfQgDRGKC7nCbjIOfQgDRGKC7nCbjlMKj3lMKj3hznkolIZhPCxqbng62rIOfQgDRGKC7nCbIOfQgDRGKC7nCbjlMKj3jlMKj3Ou5iaIBXxbojNgFByDMgHr9slX9P7EgejYZwr1aBLIOfQgDRGKC7nCbjlMKj3";
}
