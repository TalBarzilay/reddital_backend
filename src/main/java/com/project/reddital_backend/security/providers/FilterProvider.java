package com.project.reddital_backend.security.providers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.reddital_backend.security.filter.AuthenticationFilter;
import com.project.reddital_backend.security.filter.AuthorizationFilter;
import com.project.reddital_backend.security.filter.CorsFilter;
import com.project.reddital_backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;

@Component
public class FilterProvider {

    @Autowired
    private AlgorithmProvider algorithmProvider;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;



    public AuthenticationFilter getAuthentication(AuthenticationManager authenticationManager){
        return new AuthenticationFilter(authenticationManager, userService, algorithmProvider, tokenProvider, objectMapper);
    }

    public AuthorizationFilter getAuthorization() {
        return new AuthorizationFilter(algorithmProvider);
    }

    public CorsFilter getCorsFilter() {
        return new CorsFilter();
    }
}
