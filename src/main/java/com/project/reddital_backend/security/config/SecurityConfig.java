package com.project.reddital_backend.security.config;

import com.project.reddital_backend.models.User;
import com.project.reddital_backend.security.providers.FilterProvider;
import com.project.reddital_backend.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.filter.CorsFilter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String NORMAL_USER_ACCESS = "USER";
    private final String ADMIN_ACCESS       = "ADMIN";

    private final String CATEGORY_PATH = "/categories";
    private final String POST_PATH = "";

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private FilterProvider filterProvider;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable(); // ignore the default authentication method
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(filterProvider.getAuthentication(authenticationManagerBean()));
        http.addFilterBefore(filterProvider.getAuthorization(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(filterProvider.getCorsFilter(), CorsFilter.class);

        //CORS configuration
        http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
        //http.cors();


        // authorized requests
        http.authorizeRequests().antMatchers(HttpMethod.GET , CATEGORY_PATH + "/all")       .permitAll();                   // anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.POST, CATEGORY_PATH + "/create")    .hasAnyAuthority(ADMIN_ACCESS); // only admin can do this
        http.authorizeRequests().antMatchers(HttpMethod.POST, CATEGORY_PATH + "/delete")    .hasAnyAuthority(ADMIN_ACCESS); // only admin can do this
        http.authorizeRequests().antMatchers(HttpMethod.POST, CATEGORY_PATH + "/editable")  .hasAnyAuthority(ADMIN_ACCESS); // only admin can do this

        http.authorizeRequests().antMatchers(HttpMethod.POST,  "/comments/**").hasAnyAuthority(NORMAL_USER_ACCESS); //only loged user can comment


        http.authorizeRequests().antMatchers(HttpMethod.POST,  "/subredditals/**").hasAnyAuthority(ADMIN_ACCESS); // only admin can do this

        http.authorizeRequests().antMatchers(HttpMethod.GET , POST_PATH + "/posts").permitAll(); //anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.GET , POST_PATH + "/post").permitAll(); //anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.POST, POST_PATH + "/posting").hasAnyAuthority(NORMAL_USER_ACCESS); // only authenticate user can post
        http.authorizeRequests().antMatchers(HttpMethod.POST, POST_PATH + "/emote").hasAnyAuthority(NORMAL_USER_ACCESS); // only authenticate user can post
        http.authorizeRequests().antMatchers(HttpMethod.POST, POST_PATH + "/blockPost").hasAnyAuthority(ADMIN_ACCESS); // only admin can block posts
        http.authorizeRequests().antMatchers(HttpMethod.POST, POST_PATH + "/editPost").hasAnyAuthority(NORMAL_USER_ACCESS);

        http.authorizeRequests().antMatchers(HttpMethod.POST , "/login").permitAll(); //anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.POST , "/signup").permitAll(); //anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.POST , "/refreshAccessToken").permitAll(); //anyone can access this, no need to login
        http.authorizeRequests().antMatchers(HttpMethod.POST , "/blockUser").hasAnyAuthority(ADMIN_ACCESS); // only admin can block users
        http.authorizeRequests().antMatchers(HttpMethod.POST , "/changePassword").hasAuthority(NORMAL_USER_ACCESS); //anyone can acces
        http.authorizeRequests().antMatchers(HttpMethod.POST , "/updateProfile").hasAuthority(NORMAL_USER_ACCESS);


        http.authorizeRequests().antMatchers(HttpMethod.GET , "/bi").hasAnyAuthority(ADMIN_ACCESS); // only admin can see bi



        http.authorizeRequests().anyRequest().authenticated(); // authentication in all others
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    /*@Bean
    CorsConfigurationSource corsConfigurationSource () {

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();

        //config.setAllowCredentials(true);

        config.addAllowedOrigin("*");

        config.addAllowedHeader("access_token");
        config.addAllowedHeader("refresh_token");
        config.addAllowedHeader("Authorization");

        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");

        source.registerCorsConfiguration("/**", config);
        return source;
    } */
}
