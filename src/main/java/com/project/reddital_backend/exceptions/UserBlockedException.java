package com.project.reddital_backend.exceptions;


public class UserBlockedException extends CustomException {
    public UserBlockedException(String message) {
        super(message);
    }
    public UserBlockedException() {
        super("you have been blocked!");
    }
}
