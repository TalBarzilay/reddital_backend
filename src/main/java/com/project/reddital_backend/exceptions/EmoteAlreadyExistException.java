package com.project.reddital_backend.exceptions;

public class EmoteAlreadyExistException extends CustomException {
    public EmoteAlreadyExistException(String message) {
        super(message);
    }
    public EmoteAlreadyExistException() {
        super("emote on the post from this user already exist!");
    }
}