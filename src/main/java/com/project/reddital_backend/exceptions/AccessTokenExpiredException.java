package com.project.reddital_backend.exceptions;

public class AccessTokenExpiredException extends CustomException {
    public AccessTokenExpiredException(String message) {
        super(message);
    }
    public AccessTokenExpiredException() {
        super("your access token is expired");
    }
}
