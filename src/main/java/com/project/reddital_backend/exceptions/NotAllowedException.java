package com.project.reddital_backend.exceptions;


public class NotAllowedException extends CustomException {
    public NotAllowedException(String message) {
        super(message);
    }
    public NotAllowedException() {
        super("You are not allowed to perform this action!");
    }
}
