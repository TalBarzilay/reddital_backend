package com.project.reddital_backend.exceptions;

public class RefreshTokenExpiredException extends CustomException {
    public RefreshTokenExpiredException(String message) {
        super(message);
    }
    public RefreshTokenExpiredException() {
        super("your refresh token is expired");
    }
}
