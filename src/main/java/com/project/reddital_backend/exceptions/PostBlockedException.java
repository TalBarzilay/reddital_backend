package com.project.reddital_backend.exceptions;


public class PostBlockedException extends CustomException {
    public PostBlockedException(String message) {
        super(message);
    }
    public PostBlockedException() {
        super("This post is locked, cannot modify");
    }
}
