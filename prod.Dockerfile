# select parent image
FROM openjdk:11

#copy jar file
COPY /target/*.jar app.jar

# set the startup command to execute the jar
CMD [ "sh", "-c", "java $JAVA_OPTS -jar app.jar -Dserver.port=$PORT $JAR_OPTS" ]